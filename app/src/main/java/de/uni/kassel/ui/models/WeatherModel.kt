package de.uni.kassel.ui.models

import android.text.format.DateFormat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.google.android.material.card.MaterialCardView
import de.uni.kassel.R
import de.uni.kassel.util.getDrawableFromIconCode
import de.uni.kassel.util.views.CustomChartViewWeather
import java.math.RoundingMode
import kotlin.math.roundToInt

/**
 * This model shows general information (e.g. historic data and the current temperature) about the weather.
 */
@EpoxyModelClass(layout = R.layout.card_weather)
abstract class WeatherModel : EpoxyModelWithHolder<WeatherModel.Holder>() {

    @EpoxyAttribute
    var temperature: Double? = null

    @EpoxyAttribute
    var iconID: String? = null

    @EpoxyAttribute
    var humidity: Int? = null

    @EpoxyAttribute
    var pressure: Int? = null

    @EpoxyAttribute
    var uvindex: Double? = null

    @EpoxyAttribute
    var tempChartData: List<Entry>? = null

    override fun bind(holder: Holder) {
        val temperatureStr = if (temperature != null) temperature?.roundToInt().toString() else "-"
        holder.weatherTemperatureTv.text = holder.weatherTemperatureTv.context.getString(R.string.temperature_placeholder, temperatureStr)
        holder.weatherLogoTv.setImageResource(getDrawableFromIconCode(iconID))

        val humidityStr = if (humidity != null) humidity.toString() else "-"
        holder.weatherHumidityTv.text = holder.weatherHumidityTv.context.getString(R.string.percent_placeholder, humidityStr)

        holder.weatherPressureTv.text = pressure?.toString() ?: "-"
        val roundedUV = uvindex?.toBigDecimal()?.setScale(1, RoundingMode.HALF_EVEN)?.toDouble()
        holder.weatherUVIndexTv.text = roundedUV?.toString() ?: "-"

        //Color coded UV
        //https://de.wikipedia.org/wiki/UV-Index
        if (roundedUV != null) {
            val uvTV = holder.uvCircleTV
            when {
                roundedUV in 0.0..2.9 -> uvTV.strokeColor = uvTV.context.getColor(R.color.veryGreen)
                roundedUV in 2.0..5.9 -> uvTV.strokeColor = uvTV.context.getColor(R.color.sunnyYellow)
                roundedUV in 6.0..7.9 -> uvTV.strokeColor = uvTV.context.getColor(R.color.sunsetOrange)
                roundedUV in 8.0..10.9 -> uvTV.strokeColor = uvTV.context.getColor(R.color.justRed)
                roundedUV >= 11.0 -> uvTV.strokeColor = uvTV.context.getColor(R.color.prettyPurple)
            }
        }

        //Chart Settings
        if (tempChartData != null) {
            //Colors & Vars
            val tempChart = holder.tempChartTv
            val chartContext = tempChart.context
            val notComtecBlue = chartContext.getColor(R.color.notComTecBlue)
            val dataSet =
                LineDataSet(tempChartData, chartContext.getString(R.string.weather_graph_desc))
            //DataSet Settings
            dataSet.setDrawFilled(true)
            dataSet.fillColor = notComtecBlue
            dataSet.lineWidth = 4f
            dataSet.setDrawValues(false)
            dataSet.circleRadius = 3f
            dataSet.setCircleColor(notComtecBlue)
            dataSet.color = notComtecBlue
            dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
            //Data Formatter
            dataSet.setValueFormatter { value, _, _, _ ->
                chartContext.getString(R.string.temperature_placeholder, value.roundToInt().toString())
            }

            tempChart.data = LineData(dataSet)
            tempChart.xAxis.setValueFormatter { value, _ ->
                val timestamp = value.toLong()
                timestamp.let { ts ->
                    DateFormat.format("HH:mm", ts * 3600 * 1000).toString()
                }
            }
            tempChart.axisLeft.setValueFormatter { value, _ ->
                chartContext.getString(R.string.temperature_placeholder, value.toInt().toString())
            }
            tempChart.invalidate()

        }
    }

    class Holder : EpoxyHolder() {

        lateinit var weatherTemperatureTv: TextView
        lateinit var weatherLogoTv: ImageView
        lateinit var weatherHumidityTv: TextView
        lateinit var weatherPressureTv: TextView
        lateinit var weatherUVIndexTv: TextView
        lateinit var tempChartTv: CustomChartViewWeather
        lateinit var uvCircleTV: MaterialCardView

        override fun bindView(itemView: View) {
            weatherTemperatureTv = itemView.findViewById(R.id.weatherTemp)
            weatherLogoTv = itemView.findViewById(R.id.weatherLogoForecast)
            weatherHumidityTv = itemView.findViewById(R.id.weatherHumidity)
            weatherPressureTv = itemView.findViewById(R.id.weatherPressure)
            weatherUVIndexTv = itemView.findViewById(R.id.weatherUVIndex)
            tempChartTv = itemView.findViewById(R.id.tempChartTv)
            uvCircleTV = itemView.findViewById(R.id.uvCircle)
        }

    }
}
