package de.uni.kassel.ui.home

import android.content.Context
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.CarouselModel_
import com.airbnb.epoxy.EpoxyModel
import com.mapbox.mapboxsdk.geometry.LatLng
import de.uni.kassel.R
import de.uni.kassel.network.model.City
import de.uni.kassel.network.model.Model
import de.uni.kassel.network.model.custom.Sender
import de.uni.kassel.network.model.pollution.Sensor
import de.uni.kassel.ui.models.*
import de.uni.kassel.util.BaseController
import de.uni.kassel.util.getPrintableRelativeDate

private const val KASSEL = "Kassel"

/**
 * This class constructs the list of cards for the home screen.
 */
class HomeController(
    private val context: Context,
    private val favoriteListener: FavoriteCityModel.OnInteractionListener,
    private val mapListener: MapModel.OnInteractionListener,
    private val emptyListener: EmptyModel.OnInteractionListener,
    private val model: Model
) : BaseController() {

    private var loading: MutableMap<Sender, Boolean> = mutableMapOf()

    /**
     * This method is called from the epoxy library in response to call of requestModelBuild().
     * It builds a model for every card in the list.
     */
    override fun buildModels() {
        val activeCity = model.getActiveCity()

        // Add empty screen, if empty
        if (activeCity == null) {
            EmptyModel_().id("Empty")
                .callback(emptyListener)
                .addTo(this)
        } else {
            // Add favorites
            addFavorites(activeCity)

            // Add loading card, if some data is still loading
            if (loading.any { it.value }) {
                LoadingModel_()
                    .id("Loading")
                    .message(context.getString(R.string.loading))
                    .addTo(this)
            }

            addPollution(activeCity)
            addWeather(activeCity)
            addConstructions(activeCity)
            addTraffic(activeCity)

            // these features are only available for kassel
            if (model.activeCity == KASSEL) {
                addParking(activeCity)
            }

            addPollen(activeCity)
            addTweets(activeCity)

            // this one is also only available in kassel
            if (model.activeCity == KASSEL) {
                addKatWarn()
            }
        }
    }

    private fun addPollen(activeCity: City?) {
        // Pollen
        val pollen = activeCity?.pollen
        if (!pollen.isNullOrEmpty()) {
            HeaderModel_().id("pollen_header").text(context.getString(R.string.pollen_dispersal))
                .addTo(this)

            val models = mutableListOf<PollenModel_>()
            pollen.sortedByDescending { it.factor }.forEachIndexed { index, p ->
                models.add(
                    PollenModel_()
                        .id("pollen_$index")
                        .name(p.name)
                        .factor(p.factor)
                )
            }

            CarouselModel_()
                .id("pollen_carousel")
                .models(models)
                .addTo(this)
        }
    }

    private fun addConstructions(activeCity: City?) {
        val constructionSites = activeCity?.constructionSites
        if (!constructionSites.isNullOrEmpty()) {
            HeaderModel_().id("construcionshead")
                .text(context.getString(R.string.constructions)).addTo(this)

            val constructionSiteModels = mutableListOf<ConstructionModel_>()
            constructionSites.forEachIndexed { index, constructionSite ->
                constructionSiteModels.add(
                    ConstructionModel_()
                        .id("construction $index")
                        .message(constructionSite.getLongDescription())
                        .startDate(constructionSite.getStartDate())
                        .endDate(constructionSite.getEndDate())
                        .criticality(constructionSite.criticality)
                )

            }

            CarouselModel_()
                .id("construction_sites")
                .models(constructionSiteModels)
                .addTo(this)
        }
    }

    private fun addKatWarn() {
        HeaderModel_().id("katwarnheader").text(context.getString(R.string.katwarn)).addTo(this)
        KatWarnModel_()
            .id("katwarn")
            .addTo(this)
    }

    private fun addTraffic(activeCity: City?) {
        activeCity?.jamFactor?.let { jamFactor ->
            HeaderModel_().id("trafficheader").text(context.getString(R.string.traffic))
                .addTo(this)

            TrafficModel_()
                .id("traffic")
                .jamFactor(jamFactor.factor)
                .lastUpdated(jamFactor.lastUpdated)
                .addTo(this)
        }
    }

    private fun addParking(activeCity: City?) {
        val parkingSites = activeCity?.parkingSites
        if (!parkingSites.isNullOrEmpty()) {
            HeaderModel_().id("parkingheader").text(context.getString(R.string.parking))
                .addTo(this)

            val parkingSiteModels = mutableListOf<ParkingModel_>()
            parkingSites.sortedByDescending {
                it.status
            }.sortedByDescending {
                it.vacantParkingSpaces ?: -1
            }.forEachIndexed { index, parkingSite ->
                parkingSiteModels.add(
                    ParkingModel_()
                        .id("parking_$index")
                        .totalParkingCapacity(
                            parkingSite.occupiedParkingSpaces?.plus(
                                parkingSite.vacantParkingSpaces ?: 0
                            )
                        )
                        .vacantParkingSpaces(parkingSite.vacantParkingSpaces ?: 0)
                        .parkingFacilityStatus(parkingSite.status)
                        .parkingFacilityName(parkingSite.name)
                        .statusTime(parkingSite.statusTime)
                        .openingDate(parkingSite.openingDate)
                        .closingDate(parkingSite.closingDate)
                        .latLon(Pair(parkingSite.lat, parkingSite.lon))
                        .openingPeriod(parkingSite.openingPeriod)
                )
            }

            CarouselModel_()
                .id("parking_sites")
                .numViewsToShowOnScreen(1.3f)
                .models(parkingSiteModels)
                .addTo(this)

        }
    }

    private fun addWeather(activeCity: City?) {
        if (activeCity?.weather != null) {
            HeaderModel_().id("weatherheader").text(context.getString(R.string.weather_heading))
                .addTo(this)

            WeatherModel_().id("weather")
                .temperature(activeCity.weather?.main?.temp)
                .iconID(activeCity.weather?.weather?.firstOrNull()?.icon)
                .humidity(activeCity.weather?.main?.humidity)
                .pressure(activeCity.weather?.main?.pressure?.toInt())
                .tempChartData(activeCity.weather?.tempGraphEntries)
                .uvindex(activeCity.uvData?.data?.firstOrNull()?.uv)
                .addTo(this)

            //Forecast DateData
            val weatherForeCast = mutableListOf<WeatherForecastModel_>()
            activeCity.weather?.forecast?.forecastDaySummaries?.forEachIndexed { index, forecastDay ->
                weatherForeCast.add(
                    WeatherForecastModel_()
                        .id("weatherforecast $index")
                        .maxTemp(forecastDay.maxTemp)
                        .minTemp(forecastDay.minTemp)
                        .iconID(forecastDay.iconID)
                        .dayname(forecastDay.weekDayStr)
                )
            }

            CarouselModel_()
                .id("weatherforecast")
                .models(weatherForeCast)
                .padding(Carousel.Padding.dp(16, 2, 16, 8, 12))
                .addTo(this)
        }
    }

    private fun addFavorites(activeCity: City?) {
        // Favorites / Header
        val favoriteModels = mutableListOf<FavoriteCityModel_>()
        if (activeCity != null) {
            favoriteModels.add(
                FavoriteCityModel_()
                    .id("favoriteActiveCity")
                    .city(activeCity.name)
                    .listener(favoriteListener)
                    .active(true)
            )
        }

        model.favoriteCities.forEachIndexed { index, city ->
            if (city.name != activeCity?.name) {
                favoriteModels.add(
                    FavoriteCityModel_()
                        .id("favorite$index")
                        .city(city.name)
                        .listener(favoriteListener)
                )
            }
        }

        favoriteModels.add(
            FavoriteCityModel_()
                .id("add_city_automatically")
                .addCityAutomatically(true)
                .listener(favoriteListener)
        )

        favoriteModels.add(
            FavoriteCityModel_()
                .id("add_city_manually")
                .addCityManually(true)
                .listener(favoriteListener)
        )

        CarouselModel_()
            .id("favorites")
            .models(favoriteModels)
            .addTo(this)
    }

    private fun addTweets(activeCity: City?) {
        // Tweets
        val tweets = activeCity?.tweets
        if (!tweets.isNullOrEmpty()) {
            HeaderModel_().id("twitter_header").text(context.getString(R.string.current_tweets))
                .addTo(this)

            val tweetModels = mutableListOf<TweetModel_>()
            tweets.forEachIndexed { index, tweet ->
                tweetModels.add(
                    TweetModel_()
                        .id("tweet_$index")
                        .tweetId(tweet.id)
                        .profileImage(tweet.user?.getImageUrl())
                        .name(tweet.user?.username)
                        .displayName(tweet.user?.displayName)
                        .content(tweet.getTweetText())
                        .timestamp(getPrintableRelativeDate(context, tweet.getDate()))
                )
            }

            CarouselModel_()
                .id("tweets_carousel")
                .models(tweetModels)
                .addTo(this)
        }
    }

    private fun addPollution(activeCity: City?) {
        // measure stations
        activeCity?.measuringStations?.forEachIndexed { index, station ->
            if (station.measureData?.hasData() == true) {
                val measureModels = mutableListOf<EpoxyModel<*>>()

                HeaderModel_()
                    .id("stationHeader$index")
                    .text(station.name?.substringBefore(","))
                    .textToShare(station.getShareText(context))
                    .addTo(this)

                measureModels.add(
                    MeasuringStationModel_()
                        .id("generalStationInformation$index")
                        .maxValue(station.measureData?.getMaxValue())
                        .latestUpdate(station.measureData?.latestUpdate)
                )

                // NO2
                if (station.measureData?.no2History?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("no2$index${station.id}")
                            .unit(context.getString(R.string.mu_g_per_cubic_meter))
                            .data(station.measureData?.no2History)
                            .sensor(Sensor.NO2)
                    )
                }

                // PM 2.5
                if (station.measureData?.pm25History?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("pm25$index${station.id}")
                            .unit(context.getString(R.string.mu_g_per_cubic_meter))
                            .data(station.measureData?.pm25History)
                            .sensor(Sensor.PM25)
                    )
                }

                // PM 10
                if (station.measureData?.pm10History?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("pm10$index${station.id}")
                            .unit(context.getString(R.string.mu_g_per_cubic_meter))
                            .data(station.measureData?.pm10History)
                            .sensor(Sensor.PM10)
                    )
                }

                // O3
                if (station.measureData?.o3History?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("o3$index${station.id}")
                            .unit(context.getString(R.string.mu_g_per_cubic_meter))
                            .data(station.measureData?.o3History)
                            .sensor(Sensor.O3)
                    )
                }

                // SO2
                if (station.measureData?.so2History?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("so2$index${station.id}")
                            .unit(context.getString(R.string.mu_g_per_cubic_meter))
                            .data(station.measureData?.so2History)
                            .sensor(Sensor.SO2)
                    )
                }

                // Humidity
                if (station.measureData?.humidityHistory?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("humidity$index${station.id}")
                            .data(station.measureData?.humidityHistory)
                            .unit(context.getString(R.string.percent_unit))
                            .sensor(Sensor.HUMIDITY)
                            .showColorIndicator(false)
                    )
                }

                // Pressure
                if (station.measureData?.pressureHistory?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("pressure$index${station.id}")
                            .data(station.measureData?.pressureHistory)
                            .sensor(Sensor.PRESSURE)
                            .unit(context.getString(R.string.bar_unit))
                            .showColorIndicator(false)
                    )
                }

                // Temperature
                if (station.measureData?.temperatureHistory?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("temperature$index${station.id}")
                            .data(station.measureData?.temperatureHistory)
                            .sensor(Sensor.TEMPERATURE)
                            .unit(context.getString(R.string.celsius_unit))
                            .showColorIndicator(false)
                    )
                }

                // Wind
                if (station.measureData?.windHistory?.isNullOrEmpty() == false) {
                    measureModels.add(
                        ChartModel_()
                            .id("wind$index${station.id}")
                            .data(station.measureData?.windHistory)
                            .sensor(Sensor.WIND)
                            .unit(context.getString(R.string.wind_unit))
                            .showColorIndicator(false)
                    )
                }

                // location of the measuring station on a map
                measureModels.add(
                    MapModel_()
                        .id("measureStationMap$index")
                        .onInteractionListener(mapListener)
                        .geometry(LatLng(station.lattitude ?: 0.0, station.longtitude ?: 0.0))
                )

                CarouselModel_()
                    .id("stations$index")
                    .models(measureModels)
                    .addTo(this)
            }
        }
    }

    fun setLoading(loading: Boolean, sender: Sender?) {
        sender?.let {
            this.loading[sender] = loading
            requestModelBuild()
        }
    }

}
