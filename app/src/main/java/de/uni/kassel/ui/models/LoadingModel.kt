package de.uni.kassel.ui.models

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R

/**
 * This model shows a simple loading indicator.
 */
@EpoxyModelClass(layout = R.layout.card_loading)
abstract class LoadingModel : EpoxyModelWithHolder<LoadingModel.Holder>() {

    @EpoxyAttribute
    var message: String = ""

    override fun bind(holder: Holder) {
        super.bind(holder)

        holder.loadingTv.text = message
    }

    class Holder : EpoxyHolder() {

        lateinit var loadingTv: TextView

        override fun bindView(itemView: View) {
            loadingTv = itemView.findViewById(R.id.loadingTv)
        }

    }

}
