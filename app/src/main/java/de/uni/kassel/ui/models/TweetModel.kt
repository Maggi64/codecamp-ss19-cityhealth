package de.uni.kassel.ui.models

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.squareup.picasso.Picasso
import de.uni.kassel.R
import de.uni.kassel.util.CircleTransformation
import de.uni.kassel.util.openUrl

private const val TWITTER_BASE_URL = "https://twitter.com/"
private const val TWITTER_SEARCH = "${TWITTER_BASE_URL}search?q=%23"

/**
 * This model shows a tweet on card. It supports opening hashtags, user profiles and tweet details
 * in twitter.
 */
@EpoxyModelClass(layout = R.layout.card_tweet)
abstract class TweetModel : EpoxyModelWithHolder<TweetModel.Holder>() {

    @EpoxyAttribute
    var tweetId: String? = null

    @EpoxyAttribute
    var profileImage: String? = null

    @EpoxyAttribute
    var displayName: String? = null

    @EpoxyAttribute
    var name: String? = null

    @EpoxyAttribute
    var content: String? = null

    @EpoxyAttribute
    var timestamp: String? = null

    private val regex = Regex("#[\\wäöüß]+\\b")

    override fun bind(holder: Holder) {
        val context = holder.imageView.context

        // Load the profile image of the user
        Picasso.get()
            .load(profileImage)
            .transform(CircleTransformation())
            .into(holder.imageView)

        // Open the profile on twitter, when the user clicks on the profile image
        holder.imageView.setOnClickListener {
            openUrl(context, "$TWITTER_BASE_URL$name")
        }

        // Open the tweet details on twitter when the user clicks on the card or the tweet text
        holder.cardView.setOnClickListener { openTweet(context) }
        holder.messageTv.setOnClickListener { openTweet(context) }

        holder.displayNameTv.text = displayName
        holder.nameTv.text = context.getString(R.string.twitter_username, name)
        // Add highlighting for hashtags
        holder.messageTv.text = highlightHashtags(context)
        holder.messageTv.movementMethod = LinkMovementMethod.getInstance()
        holder.dateTv.text = timestamp
    }

    private fun openTweet(context: Context) {
        // show the tweet on twitter
        openUrl(context, "$TWITTER_BASE_URL$name/status/$tweetId")
    }

    private fun highlightHashtags(context: Context): SpannableString {
        val spannable = SpannableString(content)

        // find all hashtags in the tweet, because we want to highlight them
        val results = regex.findAll(content ?: "")

        results.forEach {
            try {

                // create a clickable span for every hashtag and open the search from twitter
                // with the given hashtag
                val span = object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        val twitterSearchUrl = TWITTER_SEARCH + it.value.replace("#", "")
                        openUrl(context, twitterSearchUrl)
                    }
                }

                val startIndices = it.range.first
                val endIndices = it.range.last + 1

                // highlight the hashtag with the clickable span
                spannable.setSpan(
                    span,
                    startIndices,
                    endIndices,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
            } catch (e: Exception) {
                // ignore errors
            }
        }
        return spannable
    }


    class Holder : EpoxyHolder() {

        lateinit var cardView: View
        lateinit var imageView: ImageView
        lateinit var displayNameTv: TextView
        lateinit var nameTv: TextView
        lateinit var messageTv: TextView
        lateinit var dateTv: TextView

        override fun bindView(itemView: View) {
            cardView = itemView
            imageView = itemView.findViewById(R.id.tweetIv)
            displayNameTv = itemView.findViewById(R.id.tweetDisplayNameTv)
            nameTv = itemView.findViewById(R.id.tweetNameTv)
            messageTv = itemView.findViewById(R.id.tweetContentTv)
            dateTv = itemView.findViewById(R.id.tweetDateTv)
        }

    }

}
