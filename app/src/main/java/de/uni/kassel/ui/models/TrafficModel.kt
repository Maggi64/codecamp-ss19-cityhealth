package de.uni.kassel.ui.models

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R
import de.uni.kassel.network.model.traffic.JamFactor
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * This model shows the jam factor on a card.
 */
@EpoxyModelClass(layout = R.layout.card_traffic)
abstract class TrafficModel : EpoxyModelWithHolder<TrafficModel.Holder>() {

    @EpoxyAttribute
    var jamFactor: Double? = null

    @EpoxyAttribute
    var lastUpdated: ZonedDateTime? = null

    private val lastUpdatedFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM. HH:mm")

    override fun bind(holder: Holder) {

        val context = holder.trafficJamFactor.context
        val defaultStringValue = context.getString(R.string.unknown)

        // Set jam factor
        holder.trafficJamFactor.text = context.getString(R.string.rounded_double_template, jamFactor)
        holder.trafficJamFactor.setTextColor(JamFactor.getJamColor(context, jamFactor ?: 0.0))

        // Set traffic flow
        holder.trafficSituationTv.text = JamFactor.getJamFactorText(context, jamFactor ?: 0.0).toUpperCase()
        holder.trafficSituationTv.setTextColor(JamFactor.getJamColor(context, jamFactor ?: 0.0))

        // Last updated text
        holder.trafficUpdatedTv.text = context.getString(
            R.string.last_updated, lastUpdated?.format(lastUpdatedFormatter)
                ?: defaultStringValue
        )
    }


    class Holder : EpoxyHolder() {

        lateinit var trafficJamFactor: TextView
        lateinit var trafficUpdatedTv: TextView
        lateinit var trafficSituationTv: TextView

        override fun bindView(itemView: View) {
            trafficJamFactor = itemView.findViewById(R.id.trafficJamFactor)
            trafficUpdatedTv = itemView.findViewById(R.id.trafficUpdatedTv)
            trafficSituationTv = itemView.findViewById(R.id.trafficSituationTv)
        }

    }

}
