package de.uni.kassel.ui.models

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R
import de.uni.kassel.util.shareText

/**
 * This model displays a headline for the different sections on the home screen.
 * It also supports sharing a text for the given section if the textToShare attribute is set.
 */
@EpoxyModelClass(layout = R.layout.card_headline)
abstract class HeaderModel : EpoxyModelWithHolder<HeaderModel.Holder>() {

    @EpoxyAttribute
    var text: String? = null

    @EpoxyAttribute
    var textToShare: String? = null

    override fun bind(holder: Holder) {
        holder.textView.text = text ?: ""
        if (textToShare.isNullOrBlank()) {
            holder.shareIv.setOnClickListener(null)
            holder.shareIv.visibility = View.GONE
        } else {
            holder.shareIv.visibility = View.VISIBLE
            holder.shareIv.setOnClickListener {
                textToShare?.let {
                    shareText(holder.shareIv.context, it)
                }
            }
        }
    }

    class Holder : EpoxyHolder() {

        lateinit var textView: TextView
        lateinit var shareIv: ImageView

        override fun bindView(itemView: View) {
            textView = itemView.findViewById(R.id.headlineTv)
            shareIv = itemView.findViewById(R.id.headlineShareIcon)
        }

    }

}
