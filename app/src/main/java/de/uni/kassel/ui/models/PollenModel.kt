package de.uni.kassel.ui.models

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R

private const val FACTOR_NONE = 0
private const val FACTOR_MEDIUM = 1
private const val FACTOR_STRONG = 2
private const val FACTOR_MAX = 3

/**
 * This model shows the current factor for a given pollen type.
 */
@EpoxyModelClass(layout = R.layout.card_pollen)
abstract class PollenModel : EpoxyModelWithHolder<PollenModel.Holder>() {

    @EpoxyAttribute
    var name: String? = null

    @EpoxyAttribute
    var factor: Int? = null

    override fun bind(holder: Holder) {
        holder.nameTv.text = name ?: ""
        if (factor != null) {
            val context = holder.levelTv.context
            when (factor) {
                FACTOR_NONE -> {
                    holder.levelTv.setTextColor(context.getColor(R.color.veryGreen))
                    holder.levelTv.text = context.getString(R.string.none)
                }
                FACTOR_MEDIUM -> {
                    holder.levelTv.setTextColor(context.getColor(R.color.sunnyYellow))
                    holder.levelTv.text = context.getString(R.string.weak)
                }
                FACTOR_STRONG -> {
                    holder.levelTv.setTextColor(context.getColor(R.color.sunsetOrange))
                    holder.levelTv.text = context.getString(R.string.medium)
                }
                FACTOR_MAX -> {
                    holder.levelTv.setTextColor(context.getColor(R.color.justRed))
                    holder.levelTv.text = context.getString(R.string.strong)
                }
            }
        } else {
            holder.levelTv.text = ""
        }
    }


    class Holder : EpoxyHolder() {

        lateinit var nameTv: TextView
        lateinit var levelTv: TextView

        override fun bindView(itemView: View) {
            nameTv = itemView.findViewById(R.id.nameTv)
            levelTv = itemView.findViewById(R.id.levelTv)
        }

    }

}
