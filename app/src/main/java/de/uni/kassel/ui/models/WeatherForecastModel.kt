package de.uni.kassel.ui.models

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R
import de.uni.kassel.util.getDrawableFromIconCode
import kotlin.math.roundToInt

/**
 * This model shows the weather forecast for a single day on a card.
 */
@EpoxyModelClass(layout = R.layout.card_weatherforecast)
abstract class WeatherForecastModel : EpoxyModelWithHolder<WeatherForecastModel.Holder>() {

    @EpoxyAttribute
    var dayname: String? = null

    @EpoxyAttribute
    var iconID: String? = null

    @EpoxyAttribute
    var maxTemp: Double? = null

    @EpoxyAttribute
    var minTemp: Double? = null

    override fun bind(holder: Holder) {
        holder.iconIDTv.setImageResource(getDrawableFromIconCode(iconID))

        val maxTempStr = maxTemp?.roundToInt() ?: "-"
        holder.maxTempTv.text = holder.maxTempTv.context.getString(R.string.temperature_placeholder, maxTempStr)

        val minTempStr = minTemp?.roundToInt() ?: "-"
        holder.minTempTv.text = holder.minTempTv.context.getString(R.string.temperature_placeholder, minTempStr)

        holder.daynameTv.text = dayname ?: ""
    }


    class Holder : EpoxyHolder() {

        lateinit var maxTempTv: TextView
        lateinit var minTempTv: TextView
        lateinit var iconIDTv: ImageView
        lateinit var daynameTv: TextView

        override fun bindView(itemView: View) {
            maxTempTv = itemView.findViewById(R.id.weatherTemp)
            minTempTv = itemView.findViewById(R.id.weatherMinTemp)
            iconIDTv = itemView.findViewById(R.id.weatherLogoForecast)
            daynameTv = itemView.findViewById(R.id.weatherDayName)

        }

    }

}
