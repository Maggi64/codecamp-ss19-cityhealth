package de.uni.kassel.ui.models

import android.view.View
import androidx.lifecycle.LifecycleObserver
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import de.uni.kassel.R
import de.uni.kassel.util.MapboxLifecycleObserver

private const val LOCATION_ICON = "LOCATION_ICON"

/**
 * This model shows a the given location on a map. The map is shown with the help of MapBox.
 */
@EpoxyModelClass(layout = R.layout.card_map)
abstract class MapModel : EpoxyModelWithHolder<MapModel.Holder>() {

    @EpoxyAttribute
    var geometry: LatLng = LatLng()

    @EpoxyAttribute
    var onInteractionListener: OnInteractionListener? = null

    private lateinit var observer: LifecycleObserver

    override fun bind(holder: Holder) {
        observer = MapboxLifecycleObserver(holder.mapView)

        // mapbox needs lifecycle callbacks
        onInteractionListener?.registerLifecycleObserver(observer)

        holder.mapView.getMapAsync { mapboxMap ->
            mapboxMap.setStyle(Style.TRAFFIC_DAY) {
                // Map is set up and the style has loaded
                it.addImage(LOCATION_ICON, holder.mapView.context.getDrawable(R.drawable.ic_place)!!)

                // show an icon for the measuring station
                val symbolManager = SymbolManager(holder.mapView, mapboxMap, it)
                symbolManager.iconAllowOverlap = true
                val symbol = SymbolOptions()
                    .withLatLng(geometry)
                    .withIconSize(1.5f)
                    .withIconImage(LOCATION_ICON)
                symbolManager.create(symbol)

                // zoom to the measuring station
                mapboxMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(geometry, 14.0)
                )

                // disable all gestures, because the map is shown in a recycler view and conflicts with the
                // normal scrolling behaviour
                mapboxMap.gesturesManager.moveGestureDetector.isEnabled = false
                mapboxMap.gesturesManager.multiFingerTapGestureDetector.isEnabled = false
                mapboxMap.gesturesManager.rotateGestureDetector.isEnabled = false
                mapboxMap.gesturesManager.shoveGestureDetector.isEnabled = false
                mapboxMap.gesturesManager.sidewaysShoveGestureDetector.isEnabled = false
                mapboxMap.gesturesManager.standardGestureDetector.isEnabled = false
                mapboxMap.gesturesManager.standardScaleGestureDetector.isEnabled = false
            }
        }
    }

    override fun unbind(holder: Holder) {
        super.unbind(holder)

        onInteractionListener?.unregisterLifecycleObserver(observer)
    }

    interface OnInteractionListener {

        fun registerLifecycleObserver(observer: LifecycleObserver)

        fun unregisterLifecycleObserver(observer: LifecycleObserver)

    }

    class Holder : EpoxyHolder() {

        lateinit var mapView: MapView

        override fun bindView(itemView: View) {
            mapView = itemView.findViewById(R.id.mapView)
        }

    }

}
