package de.uni.kassel.ui.models

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.google.android.material.card.MaterialCardView
import de.uni.kassel.R
import de.uni.kassel.network.model.City

/**
 * This model displays a round city image for the given favorite city.
 * The currently active favorite city will be highlighted with a red circle.
 * Additionally the model supports an add-image for adding a new favorite and a location-image for
 * adding the nearest city to the favorites.
 */
@EpoxyModelClass(layout = R.layout.card_favorite_city)
abstract class FavoriteCityModel : EpoxyModelWithHolder<FavoriteCityModel.Holder>() {

    @EpoxyAttribute
    var active: Boolean = false

    @EpoxyAttribute
    var addCityManually: Boolean = false

    @EpoxyAttribute
    var addCityAutomatically: Boolean = false

    @EpoxyAttribute
    var city: String? = null

    @EpoxyAttribute
    var listener: OnInteractionListener? = null

    override fun bind(holder: Holder) {
        when {
            addCityAutomatically -> showAddCityManually(holder)
            addCityManually -> showAddCityAutomatically(holder)
            else -> showCity(holder)
        }
    }

    private fun showAddCityAutomatically(holder: Holder) {
        holder.container.setOnLongClickListener(null)
        holder.container.setOnClickListener {
            listener?.addCityAutomatically()
        }
        holder.cardView.strokeWidth = 0

        holder.cityIv.setImageResource(R.drawable.ic_my_location)
        holder.nameTv.text = holder.nameTv.context.getString(R.string.add_location)
    }

    private fun showAddCityManually(holder: Holder) {
        holder.container.setOnLongClickListener(null)
        holder.container.setOnClickListener {
            listener?.addCityManually()
        }
        holder.cardView.strokeWidth = 0

        holder.cityIv.setImageResource(R.drawable.ic_add)
        holder.nameTv.text = holder.nameTv.context.getString(R.string.add_city)
    }

    private fun showCity(holder: Holder) {
        holder.container.setOnClickListener {
            city?.let {
                listener?.showCity(it)
            }
        }
        holder.container.setOnLongClickListener {
            city?.let {
                listener?.deleteCity(it)
            }
            true
        }
        if (active) {
            holder.cardView.strokeWidth = 6
        } else {
            holder.cardView.strokeWidth = 0
        }

        holder.cityIv.setImageResource(City.getImageForCity(city))
        holder.nameTv.text = city
    }

    interface OnInteractionListener {

        fun showCity(city: String)

        fun addCityManually()

        fun addCityAutomatically()

        fun deleteCity(city: String)

    }


    class Holder : EpoxyHolder() {

        lateinit var container: View
        lateinit var cardView: MaterialCardView
        lateinit var cityIv: ImageView
        lateinit var nameTv: TextView

        override fun bindView(itemView: View) {
            container = itemView
            cardView = itemView.findViewById(R.id.favoriteCv)
            cityIv = itemView.findViewById(R.id.favoriteIv)
            nameTv = itemView.findViewById(R.id.favoriteNameTv)
        }

    }

}
