package de.uni.kassel.ui.models

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.uni.kassel.R
import de.uni.kassel.network.model.constructions.Criticality
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

private var dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

@EpoxyModelClass(layout = R.layout.card_construction_site)
abstract class ConstructionModel : EpoxyModelWithHolder<ConstructionModel.Holder>() {

    @EpoxyAttribute
    var startDate: Date? = null

    @EpoxyAttribute
    var endDate: Date? = null

    @EpoxyAttribute
    var criticality: Criticality? = null

    @EpoxyAttribute
    var message: String? = null

    override fun bind(holder: Holder) {
        val context = holder.messageTv.context

        holder.itemView.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setTitle(R.string.constructions)
                .setMessage(message)
                .setPositiveButton(R.string.ok) { d, _ ->
                    d.dismiss()
                }
                .show()
        }

        holder.criticalityTv.text =
            context.getString(R.string.criticality, criticality?.description)
        holder.durationTv.text = context.getString(
            R.string.construction_date,
            getPrintableDate(startDate),
            getPrintableDate(endDate)
        )
        holder.messageTv.text = message
    }

    private fun getPrintableDate(date: Date?): String {
        return try {
            dateFormat.format(date)
        } catch (e: Exception) {
            "-"
        }
    }

    class Holder : EpoxyHolder() {

        lateinit var itemView: View
        lateinit var criticalityTv: TextView
        lateinit var durationTv: TextView
        lateinit var messageTv: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            criticalityTv = itemView.findViewById(R.id.constructionCriticalityTv)
            durationTv = itemView.findViewById(R.id.constructionDateTv)
            messageTv = itemView.findViewById(R.id.constructionDescTv)
        }

    }

}
