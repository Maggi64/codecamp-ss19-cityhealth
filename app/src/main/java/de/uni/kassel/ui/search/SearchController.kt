package de.uni.kassel.ui.search

import android.content.Context
import de.uni.kassel.R
import de.uni.kassel.network.model.City
import de.uni.kassel.ui.models.LoadingModel_
import de.uni.kassel.ui.models.NoResultsModel_
import de.uni.kassel.ui.models.SearchResultModel
import de.uni.kassel.ui.models.SearchResultModel_
import de.uni.kassel.util.BaseController

/**
 * This class constructs a list of search results for the city search.
 */
class SearchController(
    private val context: Context, private var callback: SearchResultModel.OnSearchResultSelectedListener
) : BaseController() {

    private var searchResults: List<City>? = null
    private var loadingMeasuringStations = false

    override fun buildModels() {
        if (loadingMeasuringStations) {
            LoadingModel_()
                .id("loading_stations")
                .message(context.getString(R.string.loading_measurement_stations))
                .addTo(this)

            return
        }

        if (searchResults.isNullOrEmpty()) {
            NoResultsModel_().id("no_results").addTo(this)
        }
        searchResults?.forEachIndexed { i, city ->
            SearchResultModel_().id("city_$i").city(city).callback(callback).addTo(this)
        }
    }

    fun updateSearchResults(searchResults: List<City>) {
        this.searchResults = searchResults
        requestModelBuild()
    }

    fun showLoadingMeasuringStations(loading: Boolean) {
        loadingMeasuringStations = loading
        requestModelBuild()
    }
}