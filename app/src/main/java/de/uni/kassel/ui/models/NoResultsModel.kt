package de.uni.kassel.ui.models

import android.view.View
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R

/**
 * This model shows a simple no result indicator for a search screen.
 */
@EpoxyModelClass(layout = R.layout.card_no_result)
abstract class NoResultsModel : EpoxyModelWithHolder<NoResultsModel.Holder>() {

    class Holder : EpoxyHolder() {

        override fun bindView(itemView: View) {
            // We don't have do bind any view here
        }

    }

}
