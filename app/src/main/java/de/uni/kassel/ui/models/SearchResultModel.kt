package de.uni.kassel.ui.models

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R
import de.uni.kassel.network.model.City

/**
 * This model shows one search result for the city search.
 */
@EpoxyModelClass(layout = R.layout.card_search_result)
abstract class SearchResultModel : EpoxyModelWithHolder<SearchResultModel.Holder>() {

    @EpoxyAttribute
    var city: City? = null

    @EpoxyAttribute
    var callback: OnSearchResultSelectedListener? = null

    override fun bind(holder: Holder) {
        holder.searchResultTv.text = city?.name ?: "-"
        holder.searchResultCv.setOnClickListener {
            city?.let { city ->
                callback?.searchResultSelected(city)
            }
        }
        holder.searchResultIv.setImageResource(City.getImageForCity(city?.name))
    }

    class Holder : EpoxyHolder() {

        lateinit var searchResultTv: TextView
        lateinit var searchResultIv: ImageView
        lateinit var searchResultCv: CardView

        override fun bindView(itemView: View) {
            searchResultTv = itemView.findViewById(R.id.searchResultTv)
            searchResultIv = itemView.findViewById(R.id.searchResultIv)
            searchResultCv = itemView.findViewById(R.id.searchResultCv)
        }

    }

    interface OnSearchResultSelectedListener {
        fun searchResultSelected(city: City)
    }

}
