package de.uni.kassel.ui.models

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R
import de.uni.kassel.network.model.parking.FacilityStatus
import de.uni.kassel.util.openGoogleMaps
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

/**
 * This model shows information about a parking facility, e.g. the number of available parking spaces.
 */
@EpoxyModelClass(layout = R.layout.card_parking)
abstract class ParkingModel : EpoxyModelWithHolder<ParkingModel.Holder>() {

    @EpoxyAttribute
    var vacantParkingSpaces: Int? = null

    @EpoxyAttribute
    var totalParkingCapacity: Int? = null

    @EpoxyAttribute
    var parkingFacilityName: String? = null

    @EpoxyAttribute
    var parkingFacilityStatus: FacilityStatus? = null

    @EpoxyAttribute
    var statusTime: OffsetDateTime? = null

    @EpoxyAttribute
    var openingDate: OffsetDateTime? = null

    @EpoxyAttribute
    var closingDate: OffsetDateTime? = null

    @EpoxyAttribute
    var openingPeriod: String? = null

    @EpoxyAttribute
    var latLon: Pair<Double, Double>? = null

    private val openingHoursFormatter = DateTimeFormatter.ofPattern("HH:mm")
    private val lastUpdatedFormatter = DateTimeFormatter.ofPattern("dd.MM. HH:mm")

    override fun bind(holder: Holder) {
        val context = holder.parkingOpenTv.context
        val defaultStringValue = context.getString(R.string.unknown)

        // open google maps when the user clicks on the card
        holder.itemView.setOnClickListener {
            latLon?.let {
                openGoogleMaps(context, it.first, it.second)
            }
        }

        // Name of the parking facility
        holder.parkingNameTv.text = parkingFacilityName ?: context.getString(R.string.fallback_parking_name)

        // Vacant parking spaces
        when (vacantParkingSpaces) {
            0 -> {
                if (parkingFacilityStatus == FacilityStatus.UNKNOWN) {
                    holder.parkingFreeSpacesTv.setTextColor(context.getColor(R.color.secondaryTextColor))
                    holder.parkingFreeSpacesTv.text = "?"
                } else {
                    holder.parkingFreeSpacesTv.setTextColor(context.getColor(R.color.justRed))
                    holder.parkingFreeSpacesTv.text = "0"
                }
            }
            in 1..20 -> {
                holder.parkingFreeSpacesTv.setTextColor(context.getColor(R.color.sunnyYellow))
                holder.parkingFreeSpacesTv.text = vacantParkingSpaces?.toString()
            }
            in 20..Int.MAX_VALUE -> {
                holder.parkingFreeSpacesTv.setTextColor(context.getColor(R.color.veryGreen))
                holder.parkingFreeSpacesTv.text = vacantParkingSpaces?.toString()
            }
            else -> {
                holder.parkingFreeSpacesTv.setTextColor(context.getColor(R.color.secondaryTextColor))
                holder.parkingFreeSpacesTv.text = "?"
            }
        }

        // Total spaces
        holder.parkingTotalSpacesTv.text = when (totalParkingCapacity) {
            0 -> {
                if (parkingFacilityStatus == FacilityStatus.UNKNOWN) {
                    "?"
                } else {
                    "0"
                }
            }
            in 1..Int.MAX_VALUE -> {
                "/ $totalParkingCapacity"
            }
            else -> {
                ""
            }
        }

        // Current status (open, closed,..)
        when (parkingFacilityStatus) {
            FacilityStatus.OPEN -> {
                holder.parkingOpenTv.text = context.getString(R.string.open).toUpperCase()
                holder.parkingOpenTv.setTextColor(context.getColor(R.color.veryGreen))
            }
            FacilityStatus.CLOSED -> {
                holder.parkingOpenTv.text = context.getString(R.string.closed).toUpperCase()
                holder.parkingOpenTv.setTextColor(context.getColor(R.color.justRed))
            }
            FacilityStatus.FULL -> {
                holder.parkingOpenTv.text = context.getString(R.string.full).toUpperCase()
                holder.parkingOpenTv.setTextColor(context.getColor(R.color.justRed))
            }
            FacilityStatus.UNKNOWN -> {
                holder.parkingOpenTv.text = defaultStringValue.toUpperCase()
                holder.parkingOpenTv.setTextColor(context.getColor(R.color.secondaryTextColor))
            }
            else -> {
                holder.parkingOpenTv.text = ""
                holder.parkingOpenTv.setTextColor(context.getColor(R.color.secondaryTextColor))
            }
        }

        // Opening period
        holder.parkingOpeningDaysTv.text = openingPeriod ?: defaultStringValue

        // Opening time
        val opening =
            context.getString(R.string.opened_from, openingDate?.format(openingHoursFormatter) ?: defaultStringValue)
        holder.parkingOpeningHourTv.text = opening

        // Closing time
        holder.parkingClosingHourTv.text =
            context.getString(
                R.string.opened_until,
                closingDate?.format(openingHoursFormatter) ?: defaultStringValue
            )

        // Last updated text
        holder.parkingUpdatedTv.text = context.getString(
            R.string.last_updated, statusTime?.format(lastUpdatedFormatter)
                ?: defaultStringValue
        )
    }


    class Holder : EpoxyHolder() {

        lateinit var itemView: View
        lateinit var parkingNameTv: TextView
        lateinit var parkingOpenTv: TextView
        lateinit var parkingFreeSpacesTv: TextView
        lateinit var parkingTotalSpacesTv: TextView
        lateinit var parkingOpeningHourTv: TextView
        lateinit var parkingClosingHourTv: TextView
        lateinit var parkingOpeningDaysTv: TextView
        lateinit var parkingUpdatedTv: TextView


        override fun bindView(itemView: View) {
            this.itemView = itemView
            parkingNameTv = itemView.findViewById(R.id.parkingNameTv)
            parkingOpenTv = itemView.findViewById(R.id.parkingOpenTv)
            parkingFreeSpacesTv = itemView.findViewById(R.id.parkingFreeSpacesTv)
            parkingTotalSpacesTv = itemView.findViewById(R.id.parkingTotalSpacesTv)
            parkingOpeningHourTv = itemView.findViewById(R.id.parkingOpeningHourTv)
            parkingClosingHourTv = itemView.findViewById(R.id.parkingClosingHourTv)
            parkingOpeningDaysTv = itemView.findViewById(R.id.parkingOpeningDaysTv)
            parkingUpdatedTv = itemView.findViewById(R.id.parkingUpdatedTv)
        }

    }

}
