package de.uni.kassel.ui.models

import android.content.Context
import android.text.format.DateFormat
import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.google.android.material.card.MaterialCardView
import de.uni.kassel.R
import de.uni.kassel.network.model.pollution.MeasureData
import de.uni.kassel.network.model.pollution.Sensor
import de.uni.kassel.util.AqiBarData
import de.uni.kassel.util.views.CustomChartView
import kotlin.math.roundToInt

/**
 * This model displays a chart view with the given data (List<BarEntry>) on a card.
 */
@EpoxyModelClass(layout = R.layout.card_chart)
abstract class ChartModel : EpoxyModelWithHolder<ChartModel.Holder>() {

    @EpoxyAttribute
    var data: List<BarEntry>? = null

    @EpoxyAttribute
    var sensor: Sensor? = null

    @EpoxyAttribute
    var showColorIndicator = true

    @EpoxyAttribute
    var unit: String? = null

    override fun bind(holder: Holder) {
        val context = holder.chartView.context
        data?.let {
            val barDataSet = BarDataSet(data, "")
            if (showColorIndicator) {
                barDataSet.colors = getColorsBasedOnValue(context, it)
            } else {
                barDataSet.color = context.getColor(R.color.notComTecBlue)
            }
            val barData = AqiBarData(barDataSet)
            holder.chartView.data = barData
            holder.chartView.xAxis.setValueFormatter { value, _ ->
                val timestamp = data?.getOrNull(value.toInt())?.data as? Long
                timestamp?.let { ts ->
                    DateFormat.format("HH:mm", ts * 1000).toString()
                } ?: ""
            }
            holder.chartDataUnitTv.text = unit ?: ""
            holder.chartView.axisLeft.setValueFormatter { value, _ ->
                "${(value).roundToInt()} ${unit ?: ""}"
            }
            holder.chartView.animateXY(1000, 1000)
            val min = it.minBy { item ->
                item.y
            }
            val max = it.maxBy { item ->
                item.y
            }
            holder.chartMinTv.text = min?.y?.toString() ?: "-"
            holder.chartMaxTv.text = max?.y?.toString() ?: "-"
            holder.chartCurrentTv.text = it.lastOrNull()?.y?.toString() ?: "-"
            if (showColorIndicator) {
                holder.chartMaxCv.strokeColor = MeasureData.getColorBasedOnValue(context, max?.y ?: 0f)
                holder.chartMinCv.strokeColor = MeasureData.getColorBasedOnValue(context, min?.y ?: 0f)
                holder.chartCurrentCv.strokeColor = MeasureData.getColorBasedOnValue(context, it.lastOrNull()?.y ?: 0f)
            } else {
                holder.chartMaxCv.strokeColor = context.getColor(R.color.white)
                holder.chartMinCv.strokeColor = context.getColor(R.color.white)
                holder.chartCurrentCv.strokeColor = context.getColor(R.color.white)
            }
        } ?: run {
            holder.chartView.clear()
        }

        sensor?.let {
            holder.chartDataDescriptionTv.text = when (sensor) {
                Sensor.SO2 -> "SO2"
                Sensor.NO2 -> "NO2"
                Sensor.PM25 -> "PM 2.5"
                Sensor.PM10 -> "PM 10"
                Sensor.HUMIDITY -> context.getString(R.string.humidity)
                Sensor.O3 -> "O3"
                Sensor.TEMPERATURE -> context.getString(R.string.temperature)
                Sensor.PRESSURE -> context.getString(R.string.pressure)
                Sensor.WIND -> context.getString(R.string.wind)
                null -> "-"
            }
        } ?: run {
            holder.chartDataDescriptionTv.text = "-"
        }
    }

    private fun getColorsBasedOnValue(context: Context, list: List<BarEntry>): List<Int> {
        val colors = mutableListOf<Int>()
        list.forEach {
            colors.add(MeasureData.getColorBasedOnValue(context, it.y))
        }
        return colors
    }

    class Holder : EpoxyHolder() {

        lateinit var chartView: CustomChartView
        lateinit var chartDataDescriptionTv: TextView
        lateinit var chartDataUnitTv: TextView
        lateinit var chartMinTv: TextView
        lateinit var chartMaxTv: TextView
        lateinit var chartCurrentTv: TextView
        lateinit var chartCurrentCv: MaterialCardView
        lateinit var chartMinCv: MaterialCardView
        lateinit var chartMaxCv: MaterialCardView

        override fun bindView(itemView: View) {
            chartCurrentTv = itemView.findViewById(R.id.chartCurrentTv)
            chartView = itemView.findViewById(R.id.chartView)
            chartDataDescriptionTv = itemView.findViewById(R.id.chartDataDescriptionTv)
            chartMinTv = itemView.findViewById(R.id.chartMinTv)
            chartMaxTv = itemView.findViewById(R.id.chartMaxTv)
            chartMaxCv = itemView.findViewById(R.id.chartMaxCv)
            chartMinCv = itemView.findViewById(R.id.chartMinCv)
            chartCurrentCv = itemView.findViewById(R.id.chartCurrentCv)
            chartDataUnitTv = itemView.findViewById(R.id.chartDataUnitTv)
        }

    }

}
