package de.uni.kassel.ui.search

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import de.uni.kassel.R
import de.uni.kassel.network.Repository
import de.uni.kassel.network.model.City
import de.uni.kassel.network.model.Model
import de.uni.kassel.network.model.custom.Resource
import de.uni.kassel.network.model.custom.Sender
import de.uni.kassel.util.Levenshtein
import de.uni.kassel.util.NetworkUtil
import de.uni.kassel.util.SharedPrefsHelper
import kotlinx.coroutines.launch
import timber.log.Timber

private const val NUM_RESULTS = 10 // maximum of results returned
private const val MAX_LEVENSHTEIN_DISTANCE = 2 // sensitive of search

/**
 * This class is responsible for search functionality, e.g. searching a city by name.
 */
class SearchViewModel(
    app: Application, private val model: Model, private val repository: Repository
) : AndroidViewModel(app) {

    val favoriteLiveData = MutableLiveData<Resource>()
    val results = MutableLiveData<List<City>>()

    /**
     * Loads the measuring stations for a city and then adds the given city and it's measuring
     * stations to the list of favorite cities.
     */
    fun addFavoriteCity(city: City) {
        val name = city.name ?: return

        if (!NetworkUtil.isNetworkAvailable(getApplication())) {
            favoriteLiveData.value = Resource.noConnection()
        } else {
            viewModelScope.launch {
                try {
                    // load the connected measuring stations for this city
                    favoriteLiveData.value = Resource.loading(Sender.ADD_FAVORITE)
                    val response = repository.searchForMeasuringStationsAsync(name).await()
                    if (response.isSuccessful) {
                        SharedPrefsHelper(getApplication()).saveFavorite(
                            name,
                            response.body()?.results
                        )

                        favoriteLiveData.value = Resource.success(Sender.ADD_FAVORITE)
                    } else {
                        favoriteLiveData.value =
                            Resource.error(R.string.generic_error_message, Sender.ADD_FAVORITE)
                    }

                } catch (e: Exception) {
                    Timber.e(e)
                    favoriteLiveData.value =
                        Resource.error(R.string.generic_error_message, Sender.ADD_FAVORITE)
                }
            }
        }
    }

    /**
     * Search for the given query.
     * This methods will search for the nearest city by location if the query is empty.
     * If the query is not empty it will use the Levenshtein distance to compute
     * the search results.
     */
    fun search(query: String?, location: Location?) {
        viewModelScope.launch {
            try {
                // Check for in-memory-cache
                if (model.cities.isEmpty()) {
                    model.cities.addAll(Model.loadAvailableCities(getApplication()))
                }
                if (query.isNullOrBlank()) {
                    // Check, if we can list the nearest locations
                    if (location != null) {
                        results.value = model.cities.toList().sortedBy {
                            val cityLocation = Location("")
                            cityLocation.latitude = it.lat ?: 0.0
                            cityLocation.longitude = it.lng ?: 0.0
                            location.distanceTo(cityLocation)
                        }.take(NUM_RESULTS)
                    } else {
                        // We can't be smart, no results.
                        results.value = listOf()
                    }
                } else {
                    // Search by simple contains
                    val startsWith = model.cities.filter {
                        it.name?.startsWith(query, ignoreCase = true) == true
                    }
                    // Check if we found anything
                    if (startsWith.isNotEmpty()) {
                        // Check for result size
                        if (startsWith.size >= NUM_RESULTS) {
                            // We don't need levenshtein
                            results.value = startsWith.take(NUM_RESULTS)
                        } else {
                            // Append some more results using levenshtein
                            val combinedResults = mutableListOf<City>()
                            combinedResults.addAll(startsWith)
                            getLevenshteinResults(query)?.let {
                                combinedResults.addAll(it)
                            }
                            results.value = combinedResults.distinctBy { it.name }.take(NUM_RESULTS)
                        }
                    } else {
                        // We did not found anything using contains, use levenshtein
                        results.value = getLevenshteinResults(query)?.take(NUM_RESULTS)
                    }

                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun getLevenshteinResults(query: String): List<City>? {
        // Search entry using levenshtein
        val levenshtein = Levenshtein()
        val distances = mutableListOf<Pair<Int, City>>()
        model.cities.forEach {
            distances.add(Pair(levenshtein.getDistance(query, it.name ?: ""), it))
        }
        return distances.filter {
            it.first <= MAX_LEVENSHTEIN_DISTANCE
        }.sortedBy {
            it.first
        }.map {
            it.second
        }
    }


}

