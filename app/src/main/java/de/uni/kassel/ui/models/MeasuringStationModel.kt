package de.uni.kassel.ui.models

import android.view.View
import android.widget.Button
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.google.android.material.card.MaterialCardView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.uni.kassel.R
import de.uni.kassel.network.model.pollution.MeasureData
import de.uni.kassel.network.model.pollution.Sensor
import de.uni.kassel.util.getPrintableRelativeDate
import java.util.*

/**
 * This model displays general information for a measuring station.
 * E.g. the highest current pollution value and the time of the last update.
 */
@EpoxyModelClass(layout = R.layout.card_measuring_station)
abstract class MeasuringStationModel : EpoxyModelWithHolder<MeasuringStationModel.Holder>() {

    @EpoxyAttribute
    var maxValue: Pair<Double, Sensor>? = null

    @EpoxyAttribute
    var latestUpdate: Date? = null

    @EpoxyAttribute
    var text: String? = null

    override fun bind(holder: Holder) {
        val context = holder.averageTv.context

        val value = maxValue?.first
        if (value != null) {
            holder.averageCv.setCardBackgroundColor(
                MeasureData.getColorBasedOnValue(context, value.toFloat())
            )
            holder.averageTv.text = value.toInt().toString()
        } else {
            holder.averageCv.setCardBackgroundColor(context.getColor(R.color.darkGrey))
            holder.averageTv.text = "-"
        }
        holder.pollutentTv.text =
            context.getString(R.string.primary_pollutant, maxValue?.second?.name ?: "")

        holder.updateTv.text = context.getString(
            R.string.latest_update, getPrintableRelativeDate(context, latestUpdate)
        )


        holder.explanationButton.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setPositiveButton(R.string.ok) { d, _ ->
                    d.dismiss()
                }
                .setView(R.layout.aqi_dialog).show()
        }

    }

    class Holder : EpoxyHolder() {

        lateinit var averageTv: TextView
        lateinit var averageCv: MaterialCardView
        lateinit var pollutentTv: TextView
        lateinit var updateTv: TextView
        lateinit var explanationButton: Button

        override fun bindView(itemView: View) {
            averageTv = itemView.findViewById(R.id.stationAverageValueTv)
            averageCv = itemView.findViewById(R.id.stationAverageValueCv)
            pollutentTv = itemView.findViewById(R.id.stationPollutantTv)
            updateTv = itemView.findViewById(R.id.stationUpdateTv)
            explanationButton = itemView.findViewById(R.id.stationButton)
        }
    }

}
