package de.uni.kassel.ui.models

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import de.uni.kassel.R


private const val URL = "https://warnungen.katwarn.de/widget/kss_plateau.png"
private const val LINK = "https://warnungen.katwarn.de/"

/**
 * This model displays the current status of katwarn for Kassel on a card.
 */
@EpoxyModelClass(layout = R.layout.card_katwarn)
abstract class KatWarnModel : EpoxyModelWithHolder<KatWarnModel.Holder>() {


    override fun bind(holder: Holder) {
        Picasso.get()
            .load(URL)
            .networkPolicy(NetworkPolicy.NO_CACHE)
            .placeholder(R.drawable.kss_placeholder)
            .into(holder.katwarnIv)
        holder.katwarnIv.setOnClickListener {
            startActivity(
                holder.katwarnIv.context,
                Intent(Intent.ACTION_VIEW, Uri.parse(LINK)),
                null
            )
        }
    }


    class Holder : EpoxyHolder() {

        lateinit var katwarnIv: ImageView

        override fun bindView(itemView: View) {
            katwarnIv = itemView.findViewById(R.id.katwarnIv)
        }

    }

}
