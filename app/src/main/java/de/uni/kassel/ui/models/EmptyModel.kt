package de.uni.kassel.ui.models

import android.view.View
import android.widget.Button
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import de.uni.kassel.R

/**
 * This model displays an empty state when no data is available.
 */
@EpoxyModelClass(layout = R.layout.card_empty)
abstract class EmptyModel : EpoxyModelWithHolder<EmptyModel.Holder>() {

    @EpoxyAttribute
    var callback: OnInteractionListener? = null

    override fun bind(holder: Holder) {
        if (callback != null) {
            holder.actionButton.setOnClickListener {
                callback?.onOpenSearchClicked()
            }
        } else {
            holder.actionButton.setOnClickListener(null)
        }

    }

    class Holder : EpoxyHolder() {

        lateinit var actionButton: Button

        override fun bindView(itemView: View) {
            actionButton = itemView.findViewById(R.id.emptyActionBtn)
        }

    }

    interface OnInteractionListener {
        fun onOpenSearchClicked()
    }

}
