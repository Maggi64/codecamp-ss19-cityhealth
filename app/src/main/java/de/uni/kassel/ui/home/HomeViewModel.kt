package de.uni.kassel.ui.home

import android.app.Application
import android.content.Context
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.uni.kassel.R
import de.uni.kassel.network.Repository
import de.uni.kassel.network.model.City
import de.uni.kassel.network.model.Model
import de.uni.kassel.network.model.custom.Resource
import de.uni.kassel.network.model.custom.Sender
import de.uni.kassel.network.model.parking.FacilityStatus
import de.uni.kassel.network.model.parking.ParkingFacility
import de.uni.kassel.network.model.parking.ParkingFacilityStatus
import de.uni.kassel.network.model.parking.ParkingSite
import de.uni.kassel.network.model.pollution.MeasuringStation
import de.uni.kassel.network.model.traffic.JamFactor
import de.uni.kassel.network.model.pollution.waqi.Waqi
import de.uni.kassel.network.model.weather.ForecastDay
import de.uni.kassel.util.NetworkUtil
import de.uni.kassel.util.SharedPrefsHelper
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.launch
import retrofit2.Response
import timber.log.Timber
import java.time.OffsetDateTime
import java.time.ZoneId

private const val SAMPLE_DATA_JSON = "sample_data.json"

/**
 * This class is responsible for getting all the needed data for the UI.
 * It loads the favorite cities and it's measuring stations from the shared prefs.
 * After that it will load all available data for the currently active city asynchronously.
 */
class HomeViewModel(
    app: Application, private val repository: Repository, private val model: Model
) : AndroidViewModel(app) {

    val data = MutableLiveData<Resource>()

    /**
     * Get the nearest city for the given location and adds it as a favorite city.
     */
    fun getNearestCity(location: Location) {
        viewModelScope.launch {
            data.value = Resource.loading(Sender.NEAREST_CITY)
            if (model.cities.isEmpty()) {
                model.cities.addAll(Model.loadAvailableCities(getApplication()))
            }
            val match = model.cities.minBy {
                val cityLocation = Location("")
                cityLocation.latitude = it.lat ?: 0.0
                cityLocation.longitude = it.lng ?: 0.0
                location.distanceTo(cityLocation)
            }
            if (match != null) {
                addFavoriteCity(match)
                data.value = Resource.success(Sender.NEAREST_CITY)
            } else {
                data.value =
                    Resource.error(R.string.generic_error_message, sender = Sender.NEAREST_CITY)
            }

            getFavorites()
        }
    }

    /**
     * Load all favorite cities and their measuring stations from the shared prefs.
     */
    fun getFavorites() {
        viewModelScope.launch {
            if (model.cities.isEmpty()) {
                model.cities.addAll(Model.loadAvailableCities(getApplication()))
            }
            val savedFavorites = SharedPrefsHelper(getApplication()).getFavoriteCityNames()
            model.favoriteCities = model.cities.filter {
                savedFavorites.contains(it.name)
            }
            model.activeCity = SharedPrefsHelper(getApplication()).getActiveCity()
            model.favoriteCities.forEach { it.loadMeasuringStations(getApplication()) }

            data.value = Resource.success(Sender.ADD_FAVORITE)

            getData()
        }
    }

    /**
     * Load all available data from various APIs for the currently active city.
     */
    fun getData() {
        if (model.getActiveCity() == null) {
            data.value = Resource.noContent()
            return
        }

        if (!NetworkUtil.isNetworkAvailable(getApplication())) {
            data.value = Resource.noConnection()
            return
        }

        loadMeasuringStations()
        loadTweetsForCity()
        loadTrafficInfo()
        loadWeatherData()
        loadConstructionSites()
        loadAvailableParkingSpots()
        loadUVData()
        loadPollenForCity()

    }

    private fun loadPollenForCity() {
        launchInScope(repository.getPollenDataAsync(model.getActiveCity()?.postcode ?: ""),
            onLoading = {
                data.value = Resource.loading(Sender.POLLEN)
            },
            onSuccess = { body ->
                model.getActiveCity()?.pollen = body?.pollen
                data.value = Resource.success(Sender.POLLEN)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.POLLEN)
            }
        )
    }

    private fun loadMeasuringStations() {
        val context: Context = getApplication()

        if (SharedPrefsHelper(context).isDemoModeActivated()) {
            // load a json with sample data if the demo modus is activated
            val json = context.assets.open(SAMPLE_DATA_JSON).bufferedReader().use { it.readText() }
            val type = object : TypeToken<Waqi>() {}.type
            val waqi: Waqi = Gson().fromJson(json, type)
            val station = MeasuringStation.getSampleStation()
            station.parseAndApplyWaqi(waqi)
            station.name = "Sample"

            model.getActiveCity()?.measuringStations?.clear()
            model.getActiveCity()?.measuringStations?.add(station)
            return
        }

        model.getActiveCity()?.measuringStations?.forEach { station ->
            launchInScope(
                repository.getWaqiPollutionDataAsync(station.id ?: ""),
                onLoading = {
                    data.value = Resource.loading(Sender.WAQI)
                },
                onSuccess = { body ->
                    station.parseAndApplyWaqi(body)
                    data.value = Resource.success(Sender.WAQI)
                },
                onError = {
                    data.value = Resource.error(R.string.generic_error_message, Sender.WAQI)
                }
            )
        }
    }

    private fun loadTweetsForCity() {
        launchInScope(repository.searchTweetsForCityAsync(model.activeCity ?: ""),
            onLoading = {
                data.value = Resource.loading(Sender.TWITTER)
            },
            onSuccess = { body ->
                model.getActiveCity()?.tweets = body?.tweets
                data.value = Resource.success(Sender.TWITTER)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.TWITTER)
            }
        )
    }

    private fun loadConstructionSites() {
        val lat = model.getActiveCity()?.lat ?: return
        val lng = model.getActiveCity()?.lng ?: return

        launchInScope(repository.getConstructionSitesAsync(lat, lng),
            onLoading = {
                data.value = Resource.loading(Sender.CONSTRUCTIONS)
            },
            onSuccess = { body ->
                model.getActiveCity()?.constructionSites = body?.trafficPage?.constructionSites
                    ?.sortedByDescending {
                        it.getCriticality().ranking
                    }?.take(50)
                data.value = Resource.success(Sender.CONSTRUCTIONS)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.CONSTRUCTIONS)
            }
        )
    }

    private fun loadTrafficInfo() {
        val lat = model.getActiveCity()?.lat ?: return
        val lng = model.getActiveCity()?.lng ?: return

        val numOfRoadsConsidered = 10 // We use the 10 worst roads and calc the mean
        launchInScope(
            repository.getTrafficInfoAsync(lat, lng),
            onLoading = {
                data.value = Resource.loading(Sender.TRAFFIC)
            },
            onSuccess = {
                var sum = 0.0
                var count = 0
                it?.roadWays?.sortedByDescending { rw ->
                    rw.flowItems?.firstOrNull()?.jamFactor
                }?.take(numOfRoadsConsidered)?.forEach { rw ->
                    rw.flowItems?.maxBy { fi ->
                        fi.jamFactor ?: 0.0
                    }?.let { fi ->
                        fi.functionClass?.let { fc ->
                            fi.jamFactor?.let { jf ->
                                if (jf >= 0 && fc < 5) {
                                    count++
                                    sum += jf
                                }
                            }
                        }
                    }
                }

                var overallJamFactor = 0.0
                if (count != 0) {
                    overallJamFactor = sum / count
                }

                val localDate = OffsetDateTime
                    .parse(it?.updated ?: "1970-01-01T00:00:00Z")
                    .atZoneSameInstant(ZoneId.systemDefault())

                model.getActiveCity()?.jamFactor = JamFactor(overallJamFactor, localDate)
                data.value = Resource.success(Sender.TRAFFIC)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.TRAFFIC)
            })
    }

    private fun loadAvailableParkingSpots() {
        // Request parking facilities
        launchInScope(repository.getParkingFacilitiesAsync(),
            onLoading = {
                data.value = Resource.loading(Sender.PARKING_FACILITIES)
            },
            onSuccess = { facilitiesRoot ->
                // Request the current status of all parking facilities
                launchInScope(repository.getParkingFacilitiesStatusAsync(),
                    onLoading = {
                        data.value = Resource.loading(Sender.PARKING)
                    },
                    onSuccess = { facilitiesStatusRoot ->
                        // Check if both are != null
                        if (facilitiesRoot != null && facilitiesStatusRoot != null) {
                            val parkingSites = mutableListOf<ParkingSite>()
                            // Match all facilities with their status by a common id
                            facilitiesRoot.parkingFacility?.forEach { facility ->
                                facilitiesStatusRoot.parkingFacilityStatus?.firstOrNull { status ->
                                    facility.id == status.id
                                }?.let { status ->
                                    // finally, add all pairs to our list of facilities
                                    parkingSites.add(parseParkingSite(status, facility))
                                }
                            }
                            // Update model
                            model.getActiveCity()?.parkingSites = parkingSites
                            data.value = Resource.success(Sender.PARKING)
                            data.value = Resource.success(Sender.PARKING_FACILITIES)
                        }
                    },
                    onError = {
                        data.value = Resource.error(R.string.generic_error_message, Sender.PARKING)
                    })
            },
            onError = {
                data.value =
                    Resource.error(R.string.generic_error_message, Sender.PARKING_FACILITIES)
            }
        )
    }

    private fun parseParkingSite(
        facilityStatus: ParkingFacilityStatus,
        facility: ParkingFacility
    ): ParkingSite {
        var statusTime: OffsetDateTime? = null
        facilityStatus.time?.let {
            statusTime = OffsetDateTime.parse(facilityStatus.time)
        }
        var openingDate: OffsetDateTime? = null
        facility.startOfOpenPeriod?.let {
            openingDate = OffsetDateTime.parse(facility.startOfOpenPeriod)
        }
        var closingDate: OffsetDateTime? = null
        facility.endOfOpenPeriod?.let {
            closingDate = OffsetDateTime.parse(facility.endOfOpenPeriod)
        }
        return ParkingSite(
            name = facility.name,
            occupancy = facilityStatus.occupancy,
            status = when (facilityStatus.status) {
                "open" -> FacilityStatus.OPEN
                "closed" -> FacilityStatus.CLOSED
                "full" -> FacilityStatus.FULL
                else -> FacilityStatus.UNKNOWN
            },
            statusTime = statusTime,
            openingDate = openingDate,
            closingDate = closingDate,
            occupiedParkingSpaces = facilityStatus.occupiedSpaces,
            vacantParkingSpaces = facilityStatus.vacantSpaces,
            lat = facility.lat?.toDoubleOrNull(),
            lon = facility.lon?.toDoubleOrNull(),
            openingPeriod = facility.openDaysOfWeek
        )
    }

    private fun loadWeatherData() {
        launchInScope(repository.getWeatherDataAsync(
            model.getActiveCity()?.lat,
            model.getActiveCity()?.lng
        ),
            onLoading = {
                data.value = Resource.loading(Sender.WEATHER)
            },
            onSuccess = {
                model.getActiveCity()?.weather = it
                data.value = Resource.success(Sender.WEATHER)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.WEATHER)
            }
        )

        launchInScope(repository.getWeatherForecastDataAsync(
            model.getActiveCity()?.lat,
            model.getActiveCity()?.lng
        ),
            onLoading = {
                data.value = Resource.loading(Sender.WEATHER_FORECAST)
            },
            onSuccess = {
                model.getActiveCity()?.weather?.forecast = it

                val forecastDayList = mutableListOf<ForecastDay>()
                for (daysAhead in 1..5) {
                    val forecastDay = ForecastDay()
                    forecastDay.calculateForecastDay(it, daysAhead)
                    forecastDayList.add(forecastDay)
                }
                model.getActiveCity()?.weather?.forecast?.forecastDaySummaries = forecastDayList
                model.getActiveCity()?.weather?.setGraphEntries(it)
                data.value = Resource.success(Sender.WEATHER_FORECAST)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.WEATHER_FORECAST)
            }
        )
    }

    private fun loadUVData() {
        launchInScope(repository.getUVDataAsync(
            model.getActiveCity()?.lat,
            model.getActiveCity()?.lng
        ),
            onLoading = {
                data.value = Resource.loading(Sender.UV)
            },
            onSuccess = {
                model.getActiveCity()?.uvData = it
                data.value = Resource.success(Sender.UV)
            },
            onError = {
                data.value = Resource.error(R.string.generic_error_message, Sender.UV)
            }
        )
    }

    /**
     * Launch the given request asynchronously with the help of kotlin coroutines.
     * The request will be launched inside the scope of the view model. This way the request will
     * be cancelled, when the user quits the app.
     */
    private fun <T> launchInScope(
        request: Deferred<Response<T>>,
        onLoading: () -> Unit,
        onSuccess: ((body: T?) -> Unit),
        onError: (() -> Unit)
    ) {
        viewModelScope.launch {
            onLoading.invoke()
            try {
                val response = request.await()
                if (response.isSuccessful) {
                    onSuccess.invoke(response.body())
                } else {
                    onError.invoke()
                }
            } catch (e: Exception) {
                onError.invoke()
                Timber.e(e)
            }
        }
    }


    private suspend fun addFavoriteCity(city: City) {
        val name = city.name ?: return
        try {
            // load the connected measuring stations for this city
            val response = repository.searchForMeasuringStationsAsync(name).await()
            if (response.isSuccessful) {
                SharedPrefsHelper(getApplication()).saveFavorite(name, response.body()?.results)
                getFavorites()
            } else {
                data.value = Resource.error(R.string.generic_error_message, Sender.ADD_FAVORITE)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

}