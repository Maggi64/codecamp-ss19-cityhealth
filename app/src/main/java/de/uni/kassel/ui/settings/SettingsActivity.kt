package de.uni.kassel.ui.settings

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import de.uni.kassel.R
import de.uni.kassel.util.SharedPrefsHelper
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    private var oldDemoModeStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setSupportActionBar(settingsToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        settingsToolbar.setNavigationOnClickListener {
            // the data on the home screen should be reloaded if the status of the demo mode changed
            if (demoStatusChanged()) {
                setResult(Activity.RESULT_OK)
            }
            finish()
        }

        val sharedPrefsHelper = SharedPrefsHelper(this)
        oldDemoModeStatus = sharedPrefsHelper.isDemoModeActivated()

        settingsSampleDataSwitch.isChecked = oldDemoModeStatus

        settingsSampleDataSwitch.setOnCheckedChangeListener { _, checked ->
            SharedPrefsHelper(this).activateDemoMode(checked)
        }

        settingsSampleDataLl.setOnClickListener {
            settingsSampleDataSwitch.isChecked = !settingsSampleDataSwitch.isChecked
        }

    }

    override fun onBackPressed() {
        if (demoStatusChanged()) {
            setResult(Activity.RESULT_OK)
        }

        super.onBackPressed()
    }

    private fun demoStatusChanged() =
        oldDemoModeStatus != SharedPrefsHelper(this).isDemoModeActivated()
}
