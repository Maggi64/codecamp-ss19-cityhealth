package de.uni.kassel.util

import java.math.RoundingMode

/**
 * Round the given double value to 2 decimal places
 */
fun roundTo2Decimal(unrouded: Double?): Double? {
    return unrouded?.toBigDecimal()?.setScale(2, RoundingMode.HALF_EVEN)?.toDouble()
}