package de.uni.kassel.util

import android.Manifest
import android.content.Context
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import de.uni.kassel.R
import java.util.ArrayList

class PermissionUtil {

    companion object {

        /**
         * Check for the runtime permission for location access.
         * Show a dialog to the user if the permission was not granted yet.
         */
        fun checkPermission(
            context: Context,
            onGranted: () -> Unit,
            onDenied: (() -> Unit)? = null
        ) {
            Permissions.check(
                context,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                R.string.permission_rationale,
                Permissions.Options()
                    .setRationaleDialogTitle(context.getString(R.string.why_location))
                    .setSettingsDialogTitle(context.getString(R.string.to_the_settings))
                    .setSettingsDialogMessage(context.getString(R.string.go_to_settings)),
                object : PermissionHandler() {
                    override fun onGranted() {
                        onGranted.invoke()
                    }

                    override fun onDenied(
                        context: Context?,
                        deniedPermissions: ArrayList<String>?
                    ) {
                        onDenied?.invoke()
                    }
                })
        }
    }

}