package de.uni.kassel.util.views

import android.content.Context
import android.util.AttributeSet
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import de.uni.kassel.R

/**
 * This custom view sets reasonable base attributes of the LineChart.
 */
class CustomChartViewWeather : LineChart {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    init {
        //Settings Chart
        val secondaryTextColor = this.context.getColor(R.color.secondaryTextColor)
        val secondaryLightColor = this.context.getColor(R.color.secondaryLightTextColor)
        this.isClickable = false
        this.isFocusable = false
        this.isActivated = false
        this.setTouchEnabled(false)
        this.setScaleEnabled(false)
        this.setDrawGridBackground(false)
        this.isHighlightPerTapEnabled = false
        this.isHighlightPerDragEnabled = false
        this.animateXY(500, 500)
        this.description.isEnabled = false
        this.legend.isEnabled = false
        this.axisRight.isEnabled = false
        this.axisLeft.textColor = secondaryTextColor
        this.axisLeft.axisLineColor = secondaryLightColor
        this.axisLeft.gridColor = secondaryLightColor
        this.xAxis.position = XAxis.XAxisPosition.BOTTOM
        this.xAxis.setLabelCount(7, true)
        this.xAxis.setDrawGridLines(false)
        this.xAxis.textColor = secondaryTextColor
        this.xAxis.axisLineColor = secondaryLightColor
    }

}