package de.uni.kassel.util

import android.Manifest
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle

const val LOCATION_VALID_NS = 3600000000000L // 60 minutes in nano seconds

/**
 * This class offers some helper methods for handling user locations.
 */
class LocationHelper {

    companion object {
        /**
         * @return true, if the location service is activated
         */
        fun isLocationEnabled(context: Context): Boolean {
            val lm = context.getSystemService(LOCATION_SERVICE) as LocationManager
            return lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        }

        /**
         * @return the last known location of the user or null if no location in the given
         * time frame can be found.
         */
        fun getLastLocationOrNull(context: Context): Location? {
            val lm = context.getSystemService(LOCATION_SERVICE) as LocationManager
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED
            ) {
                val lastLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (lastLocation != null && lastLocation.elapsedRealtimeNanos < LOCATION_VALID_NS) {
                    return null
                }
                return lastLocation
            }
            return null
        }

        /**
         * Requests the user location and forwards the location to the given callback method.
         */
        fun getLocationOneFix(context: Context, callback: (location: Location) -> Unit) {
            val lm = context.getSystemService(LOCATION_SERVICE) as LocationManager
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED
            ) {
                lm.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, object : LocationListener {
                    override fun onLocationChanged(location: Location) {
                        callback.invoke(location)
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

                    override fun onProviderEnabled(provider: String?) {}

                    override fun onProviderDisabled(provider: String?) {}

                }, null)
            }
        }

    }

}