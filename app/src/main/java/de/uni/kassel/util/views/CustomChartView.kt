package de.uni.kassel.util.views

import android.content.Context
import android.util.AttributeSet
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import de.uni.kassel.R

/**
 * This custom view sets reasonable default values for some attributes of the BarChart.
 */
class CustomChartView : BarChart {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    init {
        this.isClickable = false
        this.isFocusable = false
        this.isActivated = false
        this.setTouchEnabled(false)
        this.setScaleEnabled(false)
        this.setDrawGridBackground(false)
        this.axisRight.isEnabled = false
        this.axisRight.setDrawGridLines(false)
        this.axisLeft.setLabelCount(5, false)
        this.axisLeft.setDrawGridLines(false)
        this.axisLeft.axisLineWidth = 1f
        this.axisLeft.axisLineColor = context.getColor(R.color.secondaryLightTextColor)
        this.xAxis.setDrawGridLines(false)
        this.xAxis.setDrawAxisLine(true)
        this.xAxis.position = XAxis.XAxisPosition.BOTTOM
        this.xAxis.axisLineColor = context.getColor(R.color.secondaryLightTextColor)
        this.xAxis.axisLineWidth = 1f
        this.xAxis.labelCount = 6
        this.legend.isEnabled = false
        this.axisLeft.setDrawAxisLine(true)
        this.description.isEnabled = false
        this.axisLeft.textColor = context.getColor(R.color.secondaryTextColor)
        this.xAxis.textColor = context.getColor(R.color.secondaryTextColor)
    }

}