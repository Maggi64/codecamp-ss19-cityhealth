package de.uni.kassel.util

import android.content.Context
import android.net.ConnectivityManager

class NetworkUtil {
    companion object {
        /**
         * @return true, if a network connection is available
         */
        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val activeNetworkInfo = connectivityManager?.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
    }
}