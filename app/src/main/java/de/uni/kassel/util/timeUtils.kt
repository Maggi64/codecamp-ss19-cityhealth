package de.uni.kassel.util

import android.content.Context
import android.text.format.DateUtils
import de.uni.kassel.R
import java.util.*
import kotlin.math.absoluteValue


/**
 * Converts a [Date] to a printable relative string.
 * E.g. '5 minutes ago', '5 days ago', ...
 */
fun getPrintableRelativeDate(context: Context?, date: Date?): String? {
    if (context == null || date == null) {
        return null
    }

    if (Date().time.minus(date.time).absoluteValue < 60000) {
        return context.getString(R.string.just_now)
    }

    return DateUtils.getRelativeTimeSpanString(
        date.time,
        System.currentTimeMillis(),
        DateUtils.MINUTE_IN_MILLIS,
        DateUtils.FORMAT_ABBREV_RELATIVE
    ).toString()
}