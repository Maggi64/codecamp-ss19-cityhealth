package de.uni.kassel.util

import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyController
import de.uni.kassel.BuildConfig
import timber.log.Timber
/**
 * This base controller swallows thrown exceptions by epoxy in a production version of this application.
 * This prevents crashes and the errors will still be logged with Timber.
 */
abstract class BaseController : EpoxyController() {

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        setFilterDuplicates(true)
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        if (BuildConfig.DEBUG) {
            throw exception
        } else {
            Timber.e(exception)
        }
        super.onExceptionSwallowed(exception)
    }

}
