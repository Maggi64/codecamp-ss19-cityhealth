package de.uni.kassel.util

import de.uni.kassel.R

/**
 * Each icon code represents a specific weather situation.
 * @return the weather icon representing the weather situation
 */
fun getDrawableFromIconCode(iconCode: String?): Int {
    return when (iconCode) {
        "01d" -> R.drawable.sun
        "01n" -> R.drawable.moon
        "02d" -> R.drawable.light_cloud
        "02n" -> R.drawable.light_cloud_night
        "03d","03n","04d","04n" -> R.drawable.clouds
        "09d","09n" -> R.drawable.light_rain
        "10d","10n" -> R.drawable.rain
        "11d","11n" -> R.drawable.storm
        "13d","13n" -> R.drawable.snow
        "50d","50n" -> R.drawable.mist
        else -> R.color.darkGrey
    }

}