package de.uni.kassel.util

import android.content.Context
import de.uni.kassel.network.model.pollution.MeasuringStation

private const val SHARED_PREFS_NAME = "SharedPrefsHelper"

private const val KEY_CITIES = "Cities"
private const val KEY_ACTIVE_CITY = "KEY_ACTIVE_CITY"
private const val KEY_DEMO_MODE = "KEY_DEMO_MODE"

/**
 * This class wraps access to the shared preferences and offers simple methods to access saved values.
 */
class SharedPrefsHelper(var context: Context) {

    /**
     * Add a new city to the favorite cities.
     * This saves all measuring stations for the city, adds the city to the favorite cities and
     * sets the city as active city.
     */
    fun saveFavorite(name: String, stations: List<MeasuringStation>?) {
        // save the favorite
        val list = getFavoriteCityNames().toMutableList().apply { add(name) }
        getSharedPrefs().edit().putStringSet(KEY_CITIES, list.toSet()).apply()

        // save the measuring stations
        saveMeasuringStations(name, stations?.mapNotNull { it.id })

        // set the new favorite as active
        setActiveCity(name)
    }

    fun getFavoriteCityNames(): List<String> {
        return getSharedPrefs().getStringSet(KEY_CITIES, setOf())?.toList() ?: listOf("")
    }

    fun setActiveCity(city: String?) {
        getSharedPrefs().edit().putString(KEY_ACTIVE_CITY, city).apply()
    }

    fun getActiveCity() = getSharedPrefs().getString(KEY_ACTIVE_CITY, null) ?: ""

    private fun saveMeasuringStations(cityName: String, stations: List<String>?) {
        getSharedPrefs().edit().putStringSet(cityName, stations?.toSet()).apply()
    }

    fun getMeasuringStations(cityName: String): List<String> {
        return getSharedPrefs().getStringSet(cityName, setOf())?.toList() ?: listOf()
    }

    /**
     * Delete the city and all connected measuring stations from the shared preferences.
     */
    fun deleteCity(cityName: String) {
        getSharedPrefs().edit().remove(cityName).apply()
        val cities = getFavoriteCityNames().toMutableList().apply { remove(cityName) }
        getSharedPrefs().edit().putStringSet(KEY_CITIES, cities.toSet()).apply()
        setActiveCity(cities.firstOrNull())
    }

    fun isDemoModeActivated(): Boolean {
        return getSharedPrefs().getBoolean(KEY_DEMO_MODE, false)
    }

    fun activateDemoMode(activate: Boolean) {
        getSharedPrefs().edit().putBoolean(KEY_DEMO_MODE, activate).apply()
    }

    private fun getSharedPrefs() =
        context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
}