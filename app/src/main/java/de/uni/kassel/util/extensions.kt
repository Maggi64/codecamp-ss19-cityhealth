package de.uni.kassel.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import de.uni.kassel.CityHealthApplication
import de.uni.kassel.R

/**
 * Cast the application to the CityHealthApplication implicitly
 */
fun AppCompatActivity.getApp() = application as CityHealthApplication

/**
 * Show the soft keyboard to the user
 */
fun Activity.showKeyboard(view: View) {
    if (view.requestFocus()) {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}

/**
 * Hide the soft keyboard
 */
fun Activity.hideKeyboard() {
    currentFocus?.let {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(it.windowToken, 0)
    }
}

/**
 * Show a snackbar with the given message. If no message is provided a generic error message will be used.
 */
fun View.showSnackbar(message: String = context.getString(R.string.generic_error_message)) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}
