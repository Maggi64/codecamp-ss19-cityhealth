package de.uni.kassel.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat
import de.uni.kassel.R
import timber.log.Timber


/**
 * Open the given url. This can show a web view or directly link into other apps, e.g. twitter
 */
fun openUrl(context: Context, url: String) {
    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    try {
        context.startActivity(browserIntent)
    } catch (e: Exception) {
        Timber.e(e)
    }
}

/**
 * Share the given text via the system share dialog.
 */
fun shareText(context: Context, text: String) {
    val shareIntent = Intent()
    shareIntent.action = Intent.ACTION_SEND
    shareIntent.type = "text/plain"
    shareIntent.putExtra(Intent.EXTRA_TEXT, text)
    ContextCompat.startActivity(
        context,
        Intent.createChooser(shareIntent, context.getString(R.string.share_cityhealth)),
        null
    )
}

/**
 * Show the location identified by the given lat and lon values with google maps.
 */
fun openGoogleMaps(context: Context, lat: Double, lon: Double) {
    openUrl(context, "https://www.google.com/maps/search/?api=1&query=$lat,$lon")
}