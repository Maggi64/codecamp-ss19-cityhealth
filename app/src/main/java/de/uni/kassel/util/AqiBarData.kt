package de.uni.kassel.util

import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet

class AqiBarData(vals: BarDataSet) : BarData(vals) {
    init {
        this.setDrawValues(false)
    }
}