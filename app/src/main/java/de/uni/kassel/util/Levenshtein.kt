package de.uni.kassel.util

import kotlin.math.min

/**
 * The levenshtein distance is a distance measure between two strings.
 * We utilise for our search.
 */
class Levenshtein {

    /**
     * From https://rosettacode.org/wiki/Levenshtein_distance#Kotlin
     * Calculates the levenshtein distance between two strings.
     * @param reference string number 1
     * @param check string number 2
     * @return the levenshtein distance between reference and check.
     */
    fun getDistance(reference: String, check: String): Int {
        // degenerate cases
        if (reference == check) return 0
        if (reference == "") return check.length
        if (check == "") return reference.length

        // create two integer arrays of distances and initialize the first one
        val v0 = IntArray(check.length + 1) { it }  // previous
        val v1 = IntArray(check.length + 1)         // current

        var cost: Int
        for (i in reference.indices) {
            // calculate v1 from v0
            v1[0] = i + 1
            for (j in check.indices) {
                cost = if (reference[i] == check[j]) 0 else 1
                v1[j + 1] = min(v1[j] + 1, min(v0[j + 1] + 1, v0[j] + cost))
            }
            // copy v1 to v0 for next iteration
            for (j in 0..check.length) v0[j] = v1[j]
        }
        return v1[check.length]
    }


}

