package de.uni.kassel

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import de.uni.kassel.network.model.custom.Status
import de.uni.kassel.ui.home.HomeController
import de.uni.kassel.ui.home.HomeViewModel
import de.uni.kassel.ui.models.EmptyModel
import de.uni.kassel.ui.models.FavoriteCityModel
import de.uni.kassel.ui.models.MapModel
import de.uni.kassel.ui.settings.SettingsActivity
import de.uni.kassel.util.*
import kotlinx.android.synthetic.main.activity_main.*


private const val PICK_FAVORITES_REQUEST = 42
private const val SETTINGS_REQUEST = 52

class MainActivity : AppCompatActivity(), FavoriteCityModel.OnInteractionListener,
    MapModel.OnInteractionListener, EmptyModel.OnInteractionListener {

    private val viewModel: HomeViewModel by viewModels {
        CityHealthViewModelFactory(
            HomeViewModel(getApp(), getApp().repository, getApp().model)
        )
    }

    private var controller: HomeController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(mainToolbar)
        setTitle(R.string.app_name)

        if (controller == null) {
            controller = HomeController(
                this,
                this,
                this,
                this,
                getApp().model
            )
        }
        mainRv.adapter = controller?.adapter

        // add listener for the pull to refresh
        mainSwipeRefreshLayout.setOnRefreshListener {
            viewModel.getData()
        }

        registerObservers()

        viewModel.getFavorites()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_settings -> {
                startActivityForResult(Intent(this, SettingsActivity::class.java), SETTINGS_REQUEST)
                return true
            }
            R.id.menu_share -> {
                getApp().model.getActiveCity()?.shareCityHealth(this) ?: run {
                    mainRv.showSnackbar(getString(R.string.no_city_selected))
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onOpenSearchClicked() {
        startActivityForResult(Intent(this, SearchActivity::class.java), PICK_FAVORITES_REQUEST)
    }

    override fun addCityManually() {
        startActivityForResult(Intent(this, SearchActivity::class.java), PICK_FAVORITES_REQUEST)
    }

    override fun addCityAutomatically() {
        PermissionUtil.checkPermission(this, onGranted = {
            locateCurrentCity()
        })
    }

    override fun deleteCity(city: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.remove_city)
            .setMessage(getString(R.string.remove_city_explanation, city))
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.delete) { dialog, _ ->
                val sharedPrefs = SharedPrefsHelper(this)
                sharedPrefs.deleteCity(city)
                viewModel.getFavorites()
                dialog.dismiss()
            }
            .show()
    }

    override fun showCity(city: String) {
        getApp().model.activeCity = city
        SharedPrefsHelper(this).setActiveCity(city)
        viewModel.getData()
        controller?.requestModelBuild()
    }

    /**
     * This methods updates the shown data on the screen when the user changed something
     * in the data model in another screen. E.g. he added another city in the search screen or he
     * activated the demo mode in the settings screen.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                SETTINGS_REQUEST, PICK_FAVORITES_REQUEST -> viewModel.getFavorites()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun registerLifecycleObserver(observer: LifecycleObserver) {
        lifecycle.addObserver(observer)
    }

    override fun unregisterLifecycleObserver(observer: LifecycleObserver) {
        lifecycle.removeObserver(observer)
    }

    private fun registerObservers() {
        viewModel.data.observe(this, Observer {
            // hide swipe to refresh loading icon
            mainSwipeRefreshLayout.isRefreshing = false

            when (it.status) {
                Status.ERROR -> {
                    controller?.setLoading(false, it.sender)
                }
                Status.NO_CONTENT -> {
                    title = getString(R.string.app_name)
                    controller?.setLoading(false, it.sender)
                    controller?.requestModelBuild()
                }
                Status.LOADING -> controller?.setLoading(true, it.sender)
                Status.SUCCESS -> {
                    title = getApp().model.activeCity
                    controller?.requestModelBuild()
                    controller?.setLoading(false, it.sender)
                }
                Status.NO_CONNECTION -> {
                    AlertDialog.Builder(this).setPositiveButton(R.string.ok) { d, _ ->
                        d.dismiss()
                    }.setView(R.layout.no_internet_dialog).show()
                }
            }
        })
    }

    private fun locateCurrentCity() {
        if (LocationHelper.isLocationEnabled(this)) {
            val lastLocation = LocationHelper.getLastLocationOrNull(this)
            if (lastLocation != null) {
                // yay, use it!
                viewModel.getNearestCity(lastLocation)
            } else {
                // User has no last known location, lets request a fix async
                mainRv.showSnackbar(getString(R.string.requesting_gnss))
                LocationHelper.getLocationOneFix(this) {
                    viewModel.getNearestCity(it)
                }
            }
        } else {
            // Location is disabled
            mainRv.showSnackbar(getString(R.string.enable_gnss))
        }

    }

}
