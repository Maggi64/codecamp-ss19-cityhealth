package de.uni.kassel

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.uni.kassel.network.model.City
import de.uni.kassel.network.model.custom.Status
import de.uni.kassel.ui.models.SearchResultModel
import de.uni.kassel.ui.search.SearchController
import de.uni.kassel.ui.search.SearchViewModel
import de.uni.kassel.util.*
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity(), SearchResultModel.OnSearchResultSelectedListener {

    private val viewModel: SearchViewModel by viewModels {
        CityHealthViewModelFactory(
            SearchViewModel(getApp(), getApp().model, getApp().repository)
        )
    }

    private var controller: SearchController? = null
    private var lastSelectedCity: City? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        if (controller == null) {
            controller = SearchController(this, this)
        }

        searchRv.adapter = controller?.adapter

        registerObservers()

        // perform a query, to show nearby locations
        query()

        searchEt.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                query(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        showKeyboard(searchEt)
    }

    private fun query(s: String? = null) {
        // Only ask the user for the location, if the query is null (the first time).
        if (s == null) {
            PermissionUtil.checkPermission(this, onGranted = {
                val lastLocation = LocationHelper.getLastLocationOrNull(this@SearchActivity)
                viewModel.search(s, lastLocation)
            }, onDenied = {
                viewModel.search(s, null)
            })
        } else {
            viewModel.search(s, LocationHelper.getLastLocationOrNull(this@SearchActivity))
        }
    }

    private fun registerObservers() {
        viewModel.results.observe(this, Observer {
            controller?.updateSearchResults(it)
        })

        viewModel.favoriteLiveData.observe(this, Observer {
            when (it.status) {
                Status.ERROR -> {
                    controller?.showLoadingMeasuringStations(false)
                    showDemoModeDialog()
                }
                Status.NO_CONTENT -> controller?.showLoadingMeasuringStations(false)
                Status.SUCCESS -> {
                    setResult(RESULT_OK)
                    finish()
                }
                Status.NO_CONNECTION -> {
                    AlertDialog.Builder(this).setPositiveButton(R.string.ok) { d, _ ->
                        d.dismiss()
                    }.setView(R.layout.no_internet_dialog).show()
                }
                Status.LOADING -> controller?.showLoadingMeasuringStations(true)
            }
        })
    }

    override fun searchResultSelected(city: City) {
        hideKeyboard()
        lastSelectedCity = city
        viewModel.addFavoriteCity(city)
    }

    private fun showDemoModeDialog() {
        val cityName = lastSelectedCity?.name ?: return

        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.error)
            .setMessage(R.string.measuring_station_error)
            .setPositiveButton(R.string.activate) { dialog, _ ->
                val helper = SharedPrefsHelper(this)
                helper.saveFavorite(cityName, null)
                helper.activateDemoMode(true)
                setResult(RESULT_OK)
                finish()
                dialog.dismiss()
            }
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

}
