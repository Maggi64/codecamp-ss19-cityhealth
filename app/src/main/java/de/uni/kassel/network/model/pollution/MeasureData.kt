package de.uni.kassel.network.model.pollution

import android.content.Context
import com.github.mikephil.charting.data.BarEntry
import de.uni.kassel.R
import java.util.*


data class MeasureData(
    var currentTemperature: Double,
    var currentPressure: Double,
    var currentWind: Double,
    var currentHumidity: Double,
    var currentNo2: Double,
    var currentO3: Double,
    var currentPm10: Double,
    var currentPm25: Double,
    var currentSo2: Double,
    var temperatureHistory: List<BarEntry>,
    var pressureHistory: List<BarEntry>,
    var windHistory: List<BarEntry>,
    var humidityHistory: List<BarEntry>,
    var no2History: List<BarEntry>,
    var o3History: List<BarEntry>,
    var pm10History: List<BarEntry>,
    var pm25History: List<BarEntry>,
    var so2History: List<BarEntry>,
    var latestUpdate: Date? = null
) {
    companion object {
        fun getColorBasedOnValue(context: Context, value: Float) = when (value) {
            in 0f..50f -> context.getColor(R.color.veryGreen)
            in 50f..100f -> context.getColor(R.color.sunnyYellow)
            in 100f..150f -> context.getColor(R.color.sunsetOrange)
            in 150f..200f -> context.getColor(R.color.justRed)
            in 200f..300f -> context.getColor(R.color.prettyPurple)
            else -> context.getColor(R.color.nightRed)
        }
    }

    fun hasData(): Boolean {
        val empty = temperatureHistory.isNullOrEmpty() && pressureHistory.isNullOrEmpty() &&
                windHistory.isNullOrEmpty() && humidityHistory.isNullOrEmpty() &&
                no2History.isNullOrEmpty() && o3History.isNullOrEmpty() && pm10History.isNullOrEmpty()
                && pm25History.isNullOrEmpty() && so2History.isEmpty()
        return !empty
    }

    fun getMaxValue(): Pair<Double, Sensor> {
        var maxValue = Pair(currentNo2, Sensor.NO2)
        if (maxValue.first < currentO3) {
            maxValue = Pair(currentO3, Sensor.O3)
        }
        if (maxValue.first < currentPm10) {
            maxValue = Pair(currentPm10, Sensor.PM10)
        }
        if (maxValue.first < currentPm25) {
            maxValue = Pair(currentPm25, Sensor.PM25)
        }
        if (maxValue.first < currentSo2) {
            maxValue = Pair(currentSo2, Sensor.SO2)
        }
        return maxValue
    }
}