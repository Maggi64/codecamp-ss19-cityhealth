package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName


data class Time (
	@SerializedName("v") val v : Long?
)