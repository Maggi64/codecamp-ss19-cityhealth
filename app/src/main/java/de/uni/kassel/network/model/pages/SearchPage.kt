package de.uni.kassel.network.model.pages

import com.google.gson.annotations.SerializedName
import de.uni.kassel.network.model.pollution.MeasuringStation

data class SearchPage(
    @SerializedName("data")
    val results: List<MeasuringStation>? = null
)