package de.uni.kassel.network.model.twitter

import com.google.gson.annotations.SerializedName

data class TwitterSearchPage(
    @SerializedName("statuses")
    val tweets: List<Tweet>
)