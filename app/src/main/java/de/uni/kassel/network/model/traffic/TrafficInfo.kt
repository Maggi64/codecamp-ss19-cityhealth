package de.uni.kassel.network.model.traffic

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false, name = "TRAFFICML_REALTIME")
data class TrafficInfo @JvmOverloads constructor(
    @field:Attribute(name = "CREATED_TIMESTAMP", required = false)
    var updated: String? = null,
    @field:Path("RWS")
    @field:ElementList(
        name = "RW",
        inline = true,
        required = false
    ) var roadWays: List<RoadWay>? = null
)