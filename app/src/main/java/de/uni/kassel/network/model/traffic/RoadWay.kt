package de.uni.kassel.network.model.traffic

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false, name = "RW")
data class RoadWay @JvmOverloads constructor(
    @field:Path("FIS")
    @field:ElementList(
        name = "FI",
        inline = true,
        required = false
    ) var flowItems: List<FlowItem>? = null
)