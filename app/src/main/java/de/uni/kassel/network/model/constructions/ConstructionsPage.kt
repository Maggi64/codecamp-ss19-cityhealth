package de.uni.kassel.network.model.constructions

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

enum class ConstructionCriminality(val backendNaming: String, val ranking: Int) {
    CRITICAL("critical", 4), MAJOR("major", 3),
    MINOR("minor", 2), LOW_IMPACT("low_impact", 1),
    UNDEFINED("", 0)
}

private const val CONSTRUCTION_DESCRIPTION = "desc"
private var dateFormat = SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.getDefault())

data class ConstructionsPage(
    @SerializedName("TRAFFIC_ITEMS")
    val trafficPage: ConstructionItemPage? = null
)

data class ConstructionItemPage(
    @SerializedName("TRAFFIC_ITEM")
    val constructionSites: List<ConstructionSite>? = null
)

data class ConstructionSite(
    @SerializedName("START_TIME")
    val startTime: String? = null,
    @SerializedName("END_TIME")
    val endTime: String? = null,
    @SerializedName("CRITICALITY")
    val criticality: Criticality? = null,
    @SerializedName("TRAFFIC_ITEM_DESCRIPTION")
    val descriptions: List<ConstructionDescrition>
) {

    fun getCriticality() =
        when (criticality?.description) {
            ConstructionCriminality.CRITICAL.backendNaming -> ConstructionCriminality.CRITICAL
            ConstructionCriminality.MAJOR.backendNaming -> ConstructionCriminality.MAJOR
            ConstructionCriminality.MINOR.backendNaming -> ConstructionCriminality.MINOR
            ConstructionCriminality.LOW_IMPACT.backendNaming -> ConstructionCriminality.LOW_IMPACT
            else -> ConstructionCriminality.UNDEFINED
        }

    fun getStartDate(): Date? {
        return try {
            dateFormat.parse(startTime)
        } catch (e: Exception) {
            null
        }
    }

    fun getEndDate(): Date? {
        return try {
            dateFormat.parse(endTime)
        } catch (e: Exception) {
            null
        }
    }

    fun getLongDescription() =
        descriptions.firstOrNull {
            it.type == CONSTRUCTION_DESCRIPTION
        }?.description

}

data class Criticality(
    @SerializedName("ID")
    val id: String? = null,
    @SerializedName("DESCRIPTION")
    val description: String? = null
)

data class ConstructionDescrition(
    @SerializedName("value")
    val description: String? = null,
    @SerializedName("TYPE")
    val type: String? = null
)