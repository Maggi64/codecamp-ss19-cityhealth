package de.uni.kassel.network.model.parking

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false, name = "d2LogicalModel")
data class ParkingFacilityStatusRoot @JvmOverloads constructor(
    @field:Path("payloadPublication/genericPublicationExtension/parkingFacilityTableStatusPublication")
    @field:ElementList(
        name = "parkingFacilityStatus",
        inline = true,
        required = false
    ) var parkingFacilityStatus: List<ParkingFacilityStatus>? = null
)
