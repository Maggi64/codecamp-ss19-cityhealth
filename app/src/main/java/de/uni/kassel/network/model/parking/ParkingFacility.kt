package de.uni.kassel.network.model.parking

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false)
data class ParkingFacility @JvmOverloads constructor(
    @field:Path("parkingFacilityName/values")
    @field:Element(name = "value", required = false) var name: String? = null,
    @field:Attribute(name = "id", required = false) var id: String? = null,
    @field:Path("openingTimes/period")
    @field:Element(name = "startOfPeriod", required = false) var startOfOpenPeriod: String? = null,
    @field:Path("openingTimes/period")
    @field:Element(name = "endOfPeriod", required = false) var endOfOpenPeriod: String? = null,
    @field:Path("openingTimes/period/periodName/values")
    @field:Element(name = "value", required = false) var openDaysOfWeek: String? = null,
    @field:Path("facilityLocation/pointByCoordinates/pointCoordinates")
    @field:Element(name = "latitude", required = false) var lat: String? = null,
    @field:Path("facilityLocation/pointByCoordinates/pointCoordinates")
    @field:Element(name = "longitude", required = false) var lon: String? = null
)