package de.uni.kassel.network.model.traffic

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false, name = "FI")
data class FlowItem @JvmOverloads constructor(
    @field:Path("CF")
    @field:Attribute(name = "JF", required = false)
    var jamFactor: Double? = null,
    @field:Path("TMC")
    @field:Attribute(name = "DE", required = false)
    var streetName: String? = null,
    @field:Path("SHP")
    @field:Attribute(name = "FC", required = false)
    var functionClass: Int? = null
)