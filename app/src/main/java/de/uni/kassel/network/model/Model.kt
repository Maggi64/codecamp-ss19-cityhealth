package de.uni.kassel.network.model

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

private const val CITY_JSON = "de.json"

data class Model(
    val cities: MutableList<City> = mutableListOf(),
    var favoriteCities: List<City> = listOf(),
    var activeCity: String? = null
) {

    companion object {

        /**
         * Read the available cities from the local json file
         */
        fun loadAvailableCities(context: Context): List<City> {
            val json = context.assets.open(CITY_JSON).bufferedReader().use { it.readText() }
            val listType = object : TypeToken<List<City>>() {}.type
            return Gson().fromJson(json, listType)
        }

    }

    fun getActiveCity(): City? {
        return favoriteCities.firstOrNull {
            it.name == activeCity
        }
    }

}