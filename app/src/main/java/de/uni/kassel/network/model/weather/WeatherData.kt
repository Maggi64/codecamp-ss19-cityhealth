package de.uni.kassel.network.model.weather

import com.github.mikephil.charting.data.Entry
import java.util.*


data class WeatherData(
    val weather: List<Weather>?,
    val main: Main?,
    val dt: Int?,
    val name: String?,
    var forecast: WeatherForecastData?,
    var tempGraphEntries: List<Entry>?
) {
    fun setGraphEntries(forecastData: WeatherForecastData?, hoursAhead: Int = 24) {
        val graphEntries = mutableListOf<Entry>()
        forecastData.let {
            it?.list?.forEach { forecastEntry ->
                if (forecastEntry.dt != null && forecastEntry.main?.temp != null) {
                    val calendarStamp = GregorianCalendar()

                    val epochTS = forecastEntry.dt.toLong()
                    calendarStamp.time = Date(epochTS * 1000)

                    val calendarFutureLimit = GregorianCalendar()
                    calendarFutureLimit.add(Calendar.HOUR_OF_DAY, hoursAhead)
                    val calendarCurrent = GregorianCalendar()

                    if (calendarStamp.after(calendarCurrent) && calendarStamp.before(calendarFutureLimit)) {
                        val entry = Entry(forecastEntry.dt/3600f, forecastEntry.main.temp.toFloat())
                        graphEntries.add(entry)
                    }
                }
            }
            tempGraphEntries = graphEntries
        }
    }
}