package de.uni.kassel.network.model.custom

enum class ApiKeys(val value: String) {
    MAPBOX("pk.eyJ1IjoibGFyc21hdGh1c2VjayIsImEiOiJjanl5ZWpybTMxYXQ2M2htcHJodnE1M2tiIn0.XTIMFIzcfbNVWhU_iaK2qg"),
    AQICN("d7e3cf0b2c0ca35cfa3cec32c2779031e50a3d2b"),
    HERE_ID("rxP03hjgyPN8qtzxsWvQ"),
    HERE_CODE("nmHHqkQCU1u_ZtsD72R31A"),
    TWITTER("AAAAAAAAAAAAAAAAAAAAAHEU%2FgAAAAAA1x5UbF3vJmwIaKwbx%2FcqsUmbfIo%3DRZZhCnksvKghiqwKJI1DaEtQebYRPJvY11YDy2t7JeLSV0Uj7z"),
    OPEN_WEATHER("2cf57e5af116226a9c637c870fad256e"),
    WEATHER_BIT("e573fb1971084509938c380b71873a37")
}