package de.uni.kassel.network.model.pollution

import android.content.Context
import com.github.mikephil.charting.data.BarEntry
import com.google.gson.annotations.SerializedName
import de.uni.kassel.R
import de.uni.kassel.network.model.pollution.waqi.Iaqi
import de.uni.kassel.network.model.pollution.waqi.Waqi
import de.uni.kassel.util.getPrintableRelativeDate
import de.uni.kassel.util.roundTo2Decimal
import java.text.SimpleDateFormat
import java.util.*

private var dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

data class MeasuringStation(
    @SerializedName("uid")
    val id: String? = null,
    var name: String? = null,
    var longtitude: Double? = null,
    var lattitude: Double? = null,
    var measureData: MeasureData? = null
) {

    companion object {

        fun getSampleStation() = MeasuringStation(
            id = "sample_data",
            name = "Sample",
            longtitude = 51.313055555556,
            lattitude = 9.4925
        )

    }

    /**
     * This function provides the text that should be shared via the share function for this
     * specific measuring station.
     */
    fun getShareText(context: Context): String {
        val builder = StringBuilder()

        //Add title
        builder.append(context.getString(
            R.string.measuring_station_status, name
        ))

        // add latest update
        builder.append(context.getString(
            R.string.latest_update, getPrintableRelativeDate(context, measureData?.latestUpdate)
        ))
        builder.append("\n")

        val temperature = measureData?.currentTemperature
        if (temperature != null && temperature != 0.0) {
            builder.append(context.getString(
                R.string.current_temperature, roundTo2Decimal(temperature).toString()
            ))
        }

        val pressure = measureData?.currentPressure
        if (pressure != null && pressure != 0.0) {
            builder.append(context.getString(
                R.string.current_pressure, roundTo2Decimal(pressure).toString()
            ))
        }

        val wind = measureData?.currentWind
        if (wind != null && wind != 0.0) {
            builder.append(context.getString(
                R.string.current_wind, roundTo2Decimal(wind).toString()
            ))
        }

        val humidity = measureData?.currentHumidity
        if (humidity != null && humidity != 0.0) {
            builder.append(context.getString(
                R.string.current_humidity, roundTo2Decimal(humidity).toString()
            ))
        }

        val no2 = measureData?.currentNo2
        if (no2 != null && no2 != 0.0) {
            builder.append(context.getString(
                R.string.current_no2, roundTo2Decimal(no2).toString()
            ))
        }

        val o3 = measureData?.currentO3
        if (o3 != null && o3 != 0.0) {
            builder.append(context.getString(
                R.string.current_o3, roundTo2Decimal(o3).toString()
            ))
        }

        val pm10 = measureData?.currentPm10
        if (pm10 != null && pm10 != 0.0) {
            builder.append(context.getString(
                R.string.current_pm10, roundTo2Decimal(pm10).toString()
            ))
        }

        val pm25 = measureData?.currentPm10
        if (pm25 != null && pm25 != 0.0) {
            builder.append(context.getString(
                R.string.current_pm25, roundTo2Decimal(pm25).toString()
            ))
        }

        val so2 = measureData?.currentSo2
        if (so2 != null && so2 != 0.0) {
            builder.append(context.getString(
                R.string.current_so2, roundTo2Decimal(so2).toString()
            ))
        }

        return builder.toString()
    }

    /**
     * Parses the waqi api model to our datamodel and applies it to this station.
     * @param waqi the messy response of the waqi api.
     */
    fun parseAndApplyWaqi(waqi: Waqi?) {
        waqi?.let { w ->
            // The station name
            name = w.rxs?.obs?.first()?.msg?.city?.name
            lattitude = w.rxs?.obs?.first()?.msg?.city?.geo?.first()
            longtitude = w.rxs?.obs?.first()?.msg?.city?.geo?.getOrNull(1)
            // The stations data
            val iaqi = w.rxs?.obs?.first()?.msg?.iaqi
            iaqi?.let {
                val latestUpdate = getLatestUpdate(it)
                // Time of last update
                val currentTimestamp = w.rxs.obs.first()?.msg?.time?.v ?: 0L
                // Current humidity
                val currentHumidity =
                    (iaqi.humidity?.values?.firstOrNull() as? Double)?.div(10.0) ?: 0.0
                // Current no2. Divide by 10.0, to get the correct value.
                val currentNo2 = (iaqi.no2?.values?.firstOrNull() as? Double)?.div(10.0) ?: 0.0
                // Current o3. Divide by 10.0, to get the correct value.
                val currentO3 = (iaqi.o3?.values?.firstOrNull() as? Double)?.div(10.0) ?: 0.0
                // Current pm10
                val currentPm10 = (iaqi.pm10?.values?.firstOrNull() as? Double) ?: 0.0
                // Current pm25
                val currentPm25 = (iaqi.pm25?.values?.firstOrNull() as? Double) ?: 0.0
                // Current so2
                val currentSo2 = (iaqi.so2?.values?.firstOrNull() as? Double) ?: 0.0
                // Current temperature
                val curTemperature =
                    (iaqi.temperature?.values?.firstOrNull() as? Double)?.div(10.0) ?: 0.0
                // Current pressure
                val currentPressure =
                    (iaqi.pressure?.values?.firstOrNull() as? Double)?.div(10.0) ?: 0.0
                // Current wind
                val currentWind = (iaqi.wind?.values?.firstOrNull() as? Double)?.div(10.0) ?: 0.0
                // Apply the measure data and calculate the history
                measureData = MeasureData(
                    currentTemperature = curTemperature,
                    currentSo2 = currentSo2,
                    currentPm25 = currentPm25,
                    currentPm10 = currentPm10,
                    currentO3 = currentO3,
                    currentNo2 = currentNo2,
                    currentHumidity = currentHumidity,
                    currentPressure = currentPressure,
                    currentWind = currentWind,
                    windHistory = toHistory(
                        currentWind,
                        currentTimestamp,
                        iaqi.wind?.s,
                        iaqi.wind?.values,
                        10.0
                    ),
                    temperatureHistory = toHistory(
                        curTemperature,
                        currentTimestamp,
                        iaqi.temperature?.s,
                        iaqi.temperature?.values,
                        10.0
                    ),
                    pressureHistory = toHistory(
                        currentPressure,
                        currentTimestamp,
                        iaqi.pressure?.s,
                        iaqi.pressure?.values,
                        10.0
                    ),
                    humidityHistory = toHistory(
                        currentHumidity,
                        currentTimestamp,
                        iaqi.humidity?.s,
                        iaqi.humidity?.values,
                        10.0
                    ),
                    no2History = toHistory(
                        currentNo2,
                        currentTimestamp,
                        iaqi.no2?.s,
                        iaqi.no2?.values,
                        10.0
                    ),
                    o3History = toHistory(
                        currentO3,
                        currentTimestamp,
                        iaqi.o3?.s,
                        iaqi.o3?.values,
                        10.0
                    ),
                    pm10History = toHistory(
                        currentPm10,
                        currentTimestamp,
                        iaqi.pm10?.s,
                        iaqi.pm10?.values
                    ),
                    pm25History = toHistory(
                        currentPm25,
                        currentTimestamp,
                        iaqi.pm25?.s,
                        iaqi.pm25?.values
                    ),
                    so2History = toHistory(
                        currentSo2,
                        currentTimestamp,
                        iaqi.so2?.s,
                        iaqi.so2?.values
                    ),
                    latestUpdate = latestUpdate
                )
            }
        }
    }

    /**
     * Converts the data provided by the WAQI API to a list.
     * The WAQI API provides a list of changes, where the first element is the end-value.
     * The actually value-time pair needs to be calculated.
     * @param startValue the current value
     * @param list the list of changes
     * @param factor a division factor. Some values must be divided to get the actually result.
     * @return list of Entries to display as chart
     */
    private fun toHistory(
        startValue: Double,
        startTimestamp: Long,
        updatedTime: String?,
        list: List<Any?>?,
        factor: Double = 1.0
    ): List<BarEntry> {
        val values = mutableListOf<Pair<Double, Long>>()
        val bars = mutableListOf<BarEntry>()
        // We only want to show the last 48 hours
        list?.drop(1)?.take(48)?.let {
            // The first value is the current (and removed). Cast all other to a tuple
            val changesCasted: List<List<Double>> = it.filterIsInstance<List<Double>>()

            // Vars holding the current calculated value and time
            var current = startValue
            var currentTimestamp = startTimestamp

            // Check, if data is old and fill with 0, for missing values
            updatedTime?.let {
                val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val date = format.parse(updatedTime)
                // Calc the diff in hours
                val diff = currentTimestamp - (date.time / 1000)
                val hoursDiff = diff / 3600
                // For each missing hour, add a 0
                for (i in 2 until hoursDiff) {
                    values.add(Pair(0.0, currentTimestamp))
                    currentTimestamp -= i * 3600
                }
            }

            // Add data from the api
            changesCasted.forEachIndexed { _, c ->
                val timeDelta =
                    c.getOrNull(1)?.toLong() ?: -3600 // default should be 1 hour in seconds
                // API returns the number of hours (in seconds), since the last change.
                // Add data for each hour
                for (j in 1..(timeDelta / -3600)) {
                    values.add(Pair(current, currentTimestamp))
                    currentTimestamp -= j * 3600
                }
                current += c.getOrNull(0)?.div(factor) ?: 0.0
            }

            // Values will hold the timestamps with the values at this timestamp, but in reversed order.
            // Reverse it, to get ascending order.
            values.reverse()

            // Add the current value as last element
            values.add(Pair(startValue, startTimestamp))

            // Make sure, to take the very last 48 elements, since we display the last 48 hours.
            values.takeLast(48).forEachIndexed { i, v ->
                bars.add(BarEntry(i.toFloat(), v.first.toFloat(), v.second))
            }

        }
        return bars
    }

    private fun getLatestUpdate(iaqi: Iaqi): Date? {
        var latestUpdate = tryParsingDate(iaqi.no2?.s)
        var date = tryParsingDate(iaqi.o3?.s)
        if (compareDates(latestUpdate, date)) {
            latestUpdate = date
        }
        date = tryParsingDate(iaqi.pm10?.s)
        if (compareDates(latestUpdate, date)) {
            latestUpdate = date
        }
        date = tryParsingDate(iaqi.pm25?.s)
        if (compareDates(latestUpdate, date)) {
            latestUpdate = date
        }
        date = tryParsingDate(iaqi.so2?.s)
        if (compareDates(latestUpdate, date)) {
            latestUpdate = date
        }
        return latestUpdate
    }

    /**
     * Returns true, if the second date is the latest, false otherwise
     */
    private fun compareDates(date1: Date?, date2: Date?): Boolean {
        if (date1 == null) {
            return true
        }
        if (date2 == null) {
            return false
        }
        return date1.time < date2.time
    }

    private fun tryParsingDate(date: String?): Date? {
        if (date.isNullOrBlank()) return null

        return try {
            dateFormat.parse(date)
        } catch (e: Exception) {
            null
        }
    }

}