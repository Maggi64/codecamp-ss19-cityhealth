package de.uni.kassel.network.model.parking

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false)
data class ParkingFacilityStatus @JvmOverloads constructor(
    @field:Path("parkingFacilityReference")
    @field:Attribute(name = "id", required = false) var id: String? = null,
    @field:Element(name = "parkingFacilityOccupancy", required = false) var occupancy: Double? = null,
    @field:Element(name = "parkingFacilityStatus", required = false) var status: String? = null,
    @field:Element(name = "parkingFacilityStatusTime", required = false) var time: String? = null,
    @field:Element(name = "totalNumberOfOccupiedParkingSpaces", required = false) var occupiedSpaces: Int? = null,
    @field:Element(name = "totalNumberOfVacantParkingSpaces", required = false) var vacantSpaces: Int? = null,
    @field:Element(name = "totalParkingCapacityOverride", required = false) var capacity: Int? = null
)