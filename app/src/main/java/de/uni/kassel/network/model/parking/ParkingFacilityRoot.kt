package de.uni.kassel.network.model.parking

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false, name = "d2LogicalModel")
data class ParkingFacilityRoot @JvmOverloads constructor(
    @field:Path("payloadPublication/genericPublicationExtension/parkingFacilityTablePublication/parkingFacilityTable/parkingArea")
    @field:ElementList(
        name = "parkingFacility",
        inline = true,
        required = false
    ) var parkingFacility: List<ParkingFacility>? = null
)

