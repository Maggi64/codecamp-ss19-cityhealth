package de.uni.kassel.network.model.weather

data class WeatherList (
	val dt : Int?,
	val main : Main?,
	val weather : List<Weather>?
)