package de.uni.kassel.network

import com.google.gson.GsonBuilder
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.lang.reflect.Type

/**
 * Some APIs return a json response, others a xml response. Xml responses are marked with the
 * XmlAnnotation and need a different parser. This converter factory decides if a json parser or a
 * xml parser is needed and returns the needed parser.
 */
class XmlOrJsonConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        if (annotations.any { it.annotationClass == XML::class }) {
            @Suppress("DEPRECATION") // Currently, there is no alternative for xml parsing.
            return SimpleXmlConverterFactory.create().responseBodyConverter(type, annotations, retrofit)
        }
        return GsonConverterFactory.create(GsonBuilder().create())
            .responseBodyConverter(type, annotations, retrofit)
    }

}