package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName

data class Rxs (
	@SerializedName("obs") val obs : List<Obs?>?
)