package de.uni.kassel.network.model.pollen

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(strict = false, name = "datasets")
data class PollenInfo @JvmOverloads constructor(
    @field:Path("pollendaten/pollenbelastungen")
    @field:ElementList(
        inline = true,
        required = false
    ) var pollen: List<Pollen>? = null
)

