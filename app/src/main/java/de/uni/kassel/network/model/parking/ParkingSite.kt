package de.uni.kassel.network.model.parking

import java.time.OffsetDateTime

data class ParkingSite(
    var name: String? = null,
    var occupancy: Double? = null,
    var status: FacilityStatus? = null,
    var statusTime: OffsetDateTime? = null,
    var occupiedParkingSpaces: Int? = null,
    var vacantParkingSpaces: Int? = null,
    var openingPeriod: String? = null,
    var openingDate: OffsetDateTime? = null,
    var closingDate: OffsetDateTime? = null,
    var lat: Double? = null,
    var lon: Double? = null
)