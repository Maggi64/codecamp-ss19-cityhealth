package de.uni.kassel.network.model.custom

import androidx.annotation.StringRes

enum class Status {
    SUCCESS, ERROR, LOADING, NO_CONNECTION, NO_CONTENT
}

enum class Sender {
    WAQI, PARKING, PARKING_FACILITIES, CONSTRUCTIONS, TRAFFIC, WEATHER, WEATHER_FORECAST,
    ADD_FAVORITE, NEAREST_CITY, TWITTER, POLLEN, UV
}

/**
 * This class is used as a wrapper to send results from a view model to the connected view via
 * live data objects.
 */
data class Resource(
    val status: Status, @StringRes val message: Int? = null,
    var sender: Sender? = null
) {

    companion object {
        fun success(sender: Sender) = Resource(Status.SUCCESS, sender = sender)

        fun error(@StringRes msg: Int, sender: Sender) =
            Resource(
                Status.ERROR, msg, sender
            )

        fun noConnection() = Resource(Status.NO_CONNECTION)

        fun noContent() = Resource(Status.NO_CONTENT)

        fun loading(sender: Sender) = Resource(Status.LOADING, sender = sender)
    }

}