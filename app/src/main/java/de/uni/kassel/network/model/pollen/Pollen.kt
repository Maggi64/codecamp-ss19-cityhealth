package de.uni.kassel.network.model.pollen

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(strict = false, name="pollen")
data class Pollen @JvmOverloads constructor(
    @field:Attribute(name = "name", required = false)
    var name: String? = null,
    @field:Attribute(name = "belastung", required = false)
    var factor: Int? = null
)