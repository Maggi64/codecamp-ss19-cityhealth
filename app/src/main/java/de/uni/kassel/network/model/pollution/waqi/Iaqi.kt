package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName

data class Iaqi(
    @SerializedName("no2") val no2: Value?,
    @SerializedName("o3") val o3: Value?,
    @SerializedName("pm10") val pm10: Value?,
    @SerializedName("pm25") val pm25: Value?,
    @SerializedName("so2") val so2: Value?,
    @SerializedName("h") val humidity: Value?,
    @SerializedName("p") val pressure: Value?,
    @SerializedName("t") val temperature: Value?,
    @SerializedName("w") val wind: Value?
)