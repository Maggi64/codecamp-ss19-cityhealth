package de.uni.kassel.network.model.weather

import java.text.DateFormatSymbols
import java.util.*

data class ForecastDay(
    var minTemp: Double? = null,
    var maxTemp: Double? = null,
    var iconID: String? = null,
    var weekDayStr: String? = null
) {
    fun calculateForecastDay(forecastData: WeatherForecastData?, daysAhead: Int) {
        val iconIDs = mutableListOf<String>()

        forecastData?.list?.forEach { forecastEntry ->
            //Check if Timestamp is in the correct day
            val calendarStamp = GregorianCalendar()
            val calendarCurrent = GregorianCalendar()
            if (forecastEntry.dt?.toLong() != null) {
                val epochTS = forecastEntry.dt.toLong()
                calendarStamp.time = Date(epochTS * 1000)
            }
            if (daysAhead > 0) {
                calendarCurrent.add(Calendar.DATE, daysAhead)
            }

            if (calendarStamp.get(Calendar.YEAR) == calendarCurrent.get(Calendar.YEAR) &&
                calendarStamp.get(Calendar.MONTH) == calendarCurrent.get(Calendar.MONTH) &&
                calendarStamp.get(Calendar.DATE) == calendarCurrent.get(Calendar.DATE)
            ) {

                //getMinMaxTemp over the day
                if (forecastEntry.main?.temp != null) {
                    if (maxTemp == null) {
                        maxTemp = forecastEntry.main.temp
                    } else if (forecastEntry.main.temp > maxTemp ?: Double.MIN_VALUE) {
                        maxTemp = forecastEntry.main.temp
                    }

                    if (minTemp == null) {
                        minTemp = forecastEntry.main.temp
                    } else if (forecastEntry.main.temp < minTemp ?: Double.MAX_VALUE) {
                        minTemp = forecastEntry.main.temp
                    }
                }
                //choose icons from daytime only for forecast
                if (calendarStamp.get(Calendar.HOUR_OF_DAY) in 7..22) {
                    forecastEntry.weather?.firstOrNull()?.icon?.let { iconIDs.add(it) }
                }
            }
        }

        //Get most used Weather Icon throughout the day
        var highestID: String? = null
        if (iconIDs.isNotEmpty()) {
            var highestCount = 0
            iconIDs.groupingBy { it }.eachCount().forEach { (iconID, count) ->
                if (highestCount < count) {
                    highestID = iconID
                    highestCount = count
                }
            }
        }
        iconID = highestID

        //Get WeekDayString
        val currentDay = GregorianCalendar()

        currentDay.add(Calendar.DATE, daysAhead)
        weekDayStr = DateFormatSymbols(Locale("en")).weekdays[currentDay.get(Calendar.DAY_OF_WEEK)]

    }
}