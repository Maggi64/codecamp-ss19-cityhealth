package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName


data class Obs (
	@SerializedName("msg") val msg : Msg?
)