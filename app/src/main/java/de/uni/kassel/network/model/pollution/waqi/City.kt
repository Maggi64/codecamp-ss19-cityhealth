package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName


data class City (
	@SerializedName("geo") val geo : List<Double>?,
	@SerializedName("name") val name : String?
)