package de.uni.kassel.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import de.uni.kassel.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

private const val DUMMY_API_URL = "https://google.de/"
/**
 * This class offers a builder method for the repository class. It initializes the repository and needed
 * objects for the network communication (e.g. OkHttp and Retrofit).
 */
class ApiBuilder {

    fun initRepository(): Repository {
        val okhttp = initOkHttp()
        val api = initRetrofit(okhttp)

        return Repository(api)
    }

    private fun initRetrofit(okHttpClient: OkHttpClient) =
        Retrofit.Builder()
            .baseUrl(DUMMY_API_URL) // retrofit needs a baseUrl, just use google. We will override it with every request.
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(XmlOrJsonConverterFactory())
            .client(okHttpClient)
            .build()
            .create(CityHealthApi::class.java)

    private fun initOkHttp(): OkHttpClient {
        val client = OkHttpClient
            .Builder()

        client.connectTimeout(15, TimeUnit.SECONDS)
        client.readTimeout(15, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
            client.addInterceptor(loggingInterceptor)
        }

        return client.build()
    }

}
