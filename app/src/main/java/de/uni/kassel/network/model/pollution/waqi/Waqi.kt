package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName

data class Waqi (
	@SerializedName("rxs") val rxs : Rxs?
)