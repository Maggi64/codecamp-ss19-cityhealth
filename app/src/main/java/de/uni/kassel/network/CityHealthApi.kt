package de.uni.kassel.network

import de.uni.kassel.network.model.constructions.ConstructionsPage
import de.uni.kassel.network.model.weather.WeatherData
import de.uni.kassel.network.model.weather.WeatherForecastData
import de.uni.kassel.network.model.pages.SearchPage
import de.uni.kassel.network.model.parking.ParkingFacilityRoot
import de.uni.kassel.network.model.parking.ParkingFacilityStatusRoot
import de.uni.kassel.network.model.pollen.PollenInfo
import de.uni.kassel.network.model.traffic.TrafficInfo
import de.uni.kassel.network.model.weather.UVData
import de.uni.kassel.network.model.twitter.TwitterSearchPage
import de.uni.kassel.network.model.pollution.waqi.Waqi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

/**
 * Retrofit generates code for the network requests from this interface.
 */
interface CityHealthApi {

    @GET("https://api.openweathermap.org/data/2.5/weather/")
    fun getWeatherDataAsync(
        @Query("lat") lat: Double?,
        @Query("lon") lng: Double?,
        @Query("units") units: String,
        @Query("APPID") openweatherApiKey: String
    ): Deferred<Response<WeatherData>>

    @GET("https://api.openweathermap.org/data/2.5/forecast")
    fun getWeatherForecastDataAsync(
        @Query("lat") lat: Double?,
        @Query("lon") lng: Double?,
        @Query("units") units: String,
        @Query("APPID") openweatherApiKey: String
    ): Deferred<Response<WeatherForecastData>>

    @GET("https://api.waqi.info/search/")
    fun searchMeasuringStationsAsync(
        @Query("keyword") city: String, @Query("token") token: String
    ): Deferred<Response<SearchPage>>

    @GET("https://api.waqi.info/api/feed/@{cityId}/aqi.json")
    fun getWaqiPollutionDataAsync(@Path("cityId") cityId: String, @Query("token") token: String): Deferred<Response<Waqi>>

    @XML
    @GET("http://data-run.info/BASt-MDM-Interface/srv/2861003/clientPullService?subscriptionID=2861003")
    fun getParkingFacilitiesAsync(): Deferred<Response<ParkingFacilityRoot>>

    @XML
    @GET("http://data-run.info/BASt-MDM-Interface/srv/2861002/clientPullService?subscriptionID=2861002")
    fun getParkingFacilitiesStatusAsync(): Deferred<Response<ParkingFacilityStatusRoot>>

    @XML
    @GET("https://traffic.api.here.com/traffic/6.3/flow.xml?responseattributes=fc")
    fun getTrafficFlowDataAsync(
        @Query("app_id") appId: String,
        @Query("app_code") appCode: String,
        @Query("prox") prox: String
    ): Deferred<Response<TrafficInfo>>

    @GET("https://traffic.api.here.com/traffic/6.3/incidents.json?responseattributes=fc")
    fun getConstructionSitesAsync(
        @Query("app_id") appId: String,
        @Query("app_code") appCode: String,
        @Query("prox") prox: String
    ): Deferred<Response<ConstructionsPage>>

    @GET("https://api.weatherbit.io/v2.0/current")
    fun getUVDataAsync(
        @Query("lat") lat: Double?,
        @Query("lon") lng: Double?,
        @Query("key") apikey: String?
    ): Deferred<Response<UVData>>

    @GET("https://api.twitter.com/1.1/search/tweets.json?&result_type=mixed&tweet_mode=extended")
    fun searchTweetsForCityAsync(
        @Query("q") city: String,
        @Header("Authorization") apiKey: String
    ): Deferred<Response<TwitterSearchPage>>

    @XML
    @GET("http://www.allergie.hexal.de/pollenflug/xml-interface-neu/pollen_de_7tage.php")
    fun getPollenAsync(@Query("plz") postCode: String): Deferred<Response<PollenInfo>>
}