package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName

data class Msg (
	@SerializedName("city") val city : City?,
	@SerializedName("time") val time : Time?,
	@SerializedName("obs") val iaqi : Iaqi?
)