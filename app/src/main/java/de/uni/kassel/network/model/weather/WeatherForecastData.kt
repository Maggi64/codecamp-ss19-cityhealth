package de.uni.kassel.network.model.weather

import kotlin.collections.List

data class WeatherForecastData (
	val list : List<WeatherList>?,
	var forecastDaySummaries : MutableList<ForecastDay>
)