package de.uni.kassel.network

/**
 * This annotation is used to mark network requests that will return the values in xml syntax
 * instead of json syntax.
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class XML