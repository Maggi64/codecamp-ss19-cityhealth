package de.uni.kassel.network.model.parking

enum class FacilityStatus {
    OPEN,
    UNKNOWN,
    CLOSED,
    FULL
}