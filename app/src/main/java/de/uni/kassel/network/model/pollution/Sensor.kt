package de.uni.kassel.network.model.pollution

enum class Sensor {
    NO2,PM25,PM10,HUMIDITY,O3,TEMPERATURE,PRESSURE,WIND,SO2
}