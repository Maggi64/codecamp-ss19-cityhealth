package de.uni.kassel.network.model.traffic

import android.content.Context
import de.uni.kassel.R
import java.time.ZonedDateTime

data class JamFactor(
    val factor: Double,
    val lastUpdated: ZonedDateTime
) {
    companion object {
        /**
         * Returns the color based on the jam factor.
         * Based on https://developer.here.com/documentation/traffic/topics/tiles.html
         * @param context the app context
         */
        fun getJamColor(context: Context, jamFactor: Double): Int {
            return when {
                jamFactor < 4.0 -> context.getColor(R.color.veryGreen)
                jamFactor < 8.0 -> context.getColor(R.color.sunnyYellow)
                jamFactor < 10.0 -> context.getColor(R.color.justRed)
                else -> context.getColor(R.color.nightRed)
            }
        }

        /**
         * Returns the color based on the jam factor.
         * Based on https://developer.here.com/documentation/traffic/topics/tiles.html
         * @param context the app context
         */
        fun getJamFactorText(context: Context, jamFactor: Double): String {
            return when {
                jamFactor < 4.0 -> context.getString(R.string.free_flow)
                jamFactor < 8.0 -> context.getString(R.string.sluggish_flow)
                jamFactor < 10.0 -> context.getString(R.string.slow_flow)
                else -> context.getString(R.string.no_flow)
            }
        }
    }
}