package de.uni.kassel.network.model.weather


data class Main(
    val temp: Double? = null,
    val pressure: Double? = null,
    val humidity: Int? = null
)