package de.uni.kassel.network.model.twitter

import com.google.gson.annotations.SerializedName
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

data class Tweet(
    @SerializedName("created_at")
    val createdAt: String? = null,
    @SerializedName("id_str")
    val id: String? = null,
    @SerializedName("full_text")
    val text: String? = null,
    val user: TwitterUser? = null,
    @SerializedName("retweeted_status")
    val retweetedStatus: Tweet? = null
) {

    /**
     * Twitter extended the available characters for a tweet from 140 to 280 characters.
     * When getting a tweet, that is a retweet, the twitter API still truncates the tweet to 140 chars. This is
     * why we need to take the text from the original tweet in this case.
     */
    fun getTweetText() = retweetedStatus?.text ?: text

    fun getDate(): Date? {
        return try {
            SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH)
                .parse(createdAt)
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }
}

data class TwitterUser(
    @SerializedName("id_str")
    val id: String? = null,
    @SerializedName("name")
    val displayName: String? = null,
    @SerializedName("profile_image_url_https")
    val profileImage: String? = null,
    @SerializedName("screen_name")
    val username: String? = null
) {

    /**
     * Remove escape characters and request a higher image resolution.
     */
    fun getImageUrl() = profileImage?.replace("\\", "")?.replace("normal", "bigger")
}