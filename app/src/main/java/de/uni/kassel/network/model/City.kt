package de.uni.kassel.network.model

import android.content.Context
import com.google.gson.annotations.SerializedName
import de.uni.kassel.R
import de.uni.kassel.network.model.constructions.ConstructionSite
import de.uni.kassel.network.model.parking.ParkingSite
import de.uni.kassel.network.model.pollen.Pollen
import de.uni.kassel.network.model.pollution.MeasuringStation
import de.uni.kassel.network.model.traffic.JamFactor
import de.uni.kassel.network.model.weather.UVData
import de.uni.kassel.network.model.twitter.Tweet
import de.uni.kassel.network.model.weather.WeatherData
import de.uni.kassel.util.SharedPrefsHelper
import de.uni.kassel.util.roundTo2Decimal
import de.uni.kassel.util.shareText
import kotlin.math.roundToInt

data class City(
    @SerializedName("name", alternate = ["city"])
    var name: String?,
    var lat: Double? = null,
    var lng: Double? = null,
    var postcode: String? = null,
    var constructionSites: List<ConstructionSite>? = null,
    var parkingSites: List<ParkingSite>? = null,
    var measuringStations: MutableList<MeasuringStation>? = null,
    var jamFactor: JamFactor? = null,
    var tweets: List<Tweet>? = null,
    var weather: WeatherData? = null,
    var pollen: List<Pollen>? = null,
    var uvData: UVData? = null
) {

    companion object {
        fun getImageForCity(name: String?): Int {
            return when (name) {
                "Kassel" -> R.drawable.kassel
                "Berlin" -> R.drawable.berlin
                "Stuttgart" -> R.drawable.stuttgart
                "Frankfurt" -> R.drawable.frankfurt
                "Mannheim" -> R.drawable.mannheim
                "Hamburg" -> R.drawable.hamburg
                "Essen" -> R.drawable.essen
                "Duisburg" -> R.drawable.duisburg
                "München" -> R.drawable.munchen
                "Düsseldorf" -> R.drawable.dusseldorf
                "Köln" -> R.drawable.koln
                "Wuppertal" -> R.drawable.wuppertal
                "Saarbrücken" -> R.drawable.saarbrucken
                "Bremen" -> R.drawable.bremen
                "Hannover" -> R.drawable.hannover
                "Bonn" -> R.drawable.bonn
                "Dresden" -> R.drawable.dresden
                "Wiesbaden" -> R.drawable.wiesbaden
                "Dortmund" -> R.drawable.dortmund
                "Leipzig" -> R.drawable.leipzig
                "Heidelberg" -> R.drawable.heidelberg
                "Karlsruhe" -> R.drawable.karlsruhe
                "Augsburg" -> R.drawable.augsburg
                "Bielefeld" -> R.drawable.bielefeld
                "Koblenz" -> R.drawable.koblenz
                "Altchemnitz" -> R.drawable.altchemnitz

                else -> R.drawable.image_city
            }
        }
    }

    /**
     * This function shares the current city health status via text
     */
    fun shareCityHealth(context: Context) {
        var shareBody = context.getString(R.string.current_cityhealth, name)

        //Temperature & Humidity
        if (weather != null) {
            shareBody += "${context.getString(R.string.temperature)}: ${context.getString(
                R.string.temperature_placeholder,
                weather?.main?.temp?.roundToInt().toString()
            )}\n" +
                    "${context.getString(R.string.humidity)}: ${context.getString(
                        R.string.percent_placeholder,
                        weather?.main?.humidity.toString()
                    )}\n"
        }
        //UV-Index
        if (uvData?.data?.firstOrNull()?.uv != null) {
            shareBody += context.getString(R.string.uvindex) + ": ${roundTo2Decimal(uvData?.data?.firstOrNull()?.uv)}\n"
        }
        //Traffic
        if (jamFactor != null) {
            shareBody += context.getString(R.string.jam_factor) + ": ${roundTo2Decimal(jamFactor?.factor)} (0-10)\n"
        }
        shareBody += context.getString(R.string.share_info)

        shareText(context, shareBody)
    }

    fun loadMeasuringStations(context: Context) {
        val name = name ?: return

        measuringStations = SharedPrefsHelper(context)
            .getMeasuringStations(name)
            .map { MeasuringStation(id = it) }
            .toMutableList()
    }

}
