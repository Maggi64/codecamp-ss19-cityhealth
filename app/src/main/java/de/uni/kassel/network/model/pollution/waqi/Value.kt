package de.uni.kassel.network.model.pollution.waqi

import com.google.gson.annotations.SerializedName

data class Value(
    @SerializedName("s") val s: String?,
    @SerializedName("v") val values: List<Any?>?
)