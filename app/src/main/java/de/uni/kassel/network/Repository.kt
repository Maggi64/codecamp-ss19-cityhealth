package de.uni.kassel.network

import de.uni.kassel.network.model.custom.ApiKeys

class Repository(
    private val api: CityHealthApi
) {

    fun searchForMeasuringStationsAsync(city: String) =
        api.searchMeasuringStationsAsync(city, ApiKeys.AQICN.value)

    fun getWaqiPollutionDataAsync(cityId: String) =
        api.getWaqiPollutionDataAsync(cityId, ApiKeys.AQICN.value)

    fun getParkingFacilitiesAsync() = api.getParkingFacilitiesAsync()

    fun getParkingFacilitiesStatusAsync() = api.getParkingFacilitiesStatusAsync()

    fun getTrafficInfoAsync(
        lat: Double,
        lon: Double,
        radius: Int = 3000 // 3 km default
    ) = api.getTrafficFlowDataAsync(
        ApiKeys.HERE_ID.value,
        ApiKeys.HERE_CODE.value,
        "$lat,$lon,$radius"
    )

    fun getConstructionSitesAsync(
        lat: Double,
        lon: Double,
        radius: Int = 3000 // 3 km default
    ) = api.getConstructionSitesAsync(
        ApiKeys.HERE_ID.value,
        ApiKeys.HERE_CODE.value,
        "$lat,$lon,$radius"
    )

    fun searchTweetsForCityAsync(city: String) =
        api.searchTweetsForCityAsync("%23$city", getOAuthHeader(ApiKeys.TWITTER.value))

    fun getWeatherDataAsync(lat: Double?, lng: Double?) =
        api.getWeatherDataAsync(lat, lng, "metric", ApiKeys.OPEN_WEATHER.value)

    fun getWeatherForecastDataAsync(lat: Double?, lng: Double?) =
        api.getWeatherForecastDataAsync(lat, lng, "metric", ApiKeys.OPEN_WEATHER.value)

    fun getPollenDataAsync(postCode: String) =
        api.getPollenAsync(postCode)

    fun getUVDataAsync(lat: Double?, lng: Double?) =
        api.getUVDataAsync(lat, lng, ApiKeys.WEATHER_BIT.value)

    private fun getOAuthHeader(apiKey: String) = "Bearer $apiKey"

}