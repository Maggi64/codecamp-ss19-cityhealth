package de.uni.kassel

import android.app.Application
import com.mapbox.mapboxsdk.Mapbox
import de.uni.kassel.network.ApiBuilder
import de.uni.kassel.network.Repository
import de.uni.kassel.network.model.Model
import de.uni.kassel.network.model.custom.ApiKeys
import timber.log.Timber

class CityHealthApplication : Application() {

    lateinit var repository: Repository
    val model = Model()

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            // Add a logger only for debug builds. This will log information about errors and
            // all network requests on the console.
            Timber.plant(Timber.DebugTree())
        }

        // Initialise the needed classes for getting data from the various APIs
        repository = ApiBuilder().initRepository()

        // Initialise Mapbox
        Mapbox.getInstance(this, ApiKeys.MAPBOX.value)
    }

}